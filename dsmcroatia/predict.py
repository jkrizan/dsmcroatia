#%%
from pathlib import Path
from utils import PreProcess, raster_n_tiles, make_window, ttprint, raster_get_profile, log_unpack
from settings import fld_models_root, fn_models_pp, fld_data_predictions, fld_data_root
from settings import targets, class_targets, predict_window_dim, non_logtrans, get_predictors_layers

import numpy as np
import rasterio as rio
from multiprocessing.pool import ThreadPool
import pickle
import os, sys
import pandas as pd
import gc


# fixes os.exec* import errors...
_working_dir = Path(__file__).parent.parent.resolve().as_posix()
sys.path.append(_working_dir)

OUTPUT_NODATA = -9999
OUTPUT_DTYPE = np.float32

OUT_PROFILE = raster_get_profile(
    nodata=OUTPUT_NODATA,
    dtype=OUTPUT_DTYPE,
)



#%%

def read_layer(window, mask):
    def wrapped(fn):
        try:
            with rio.open(fn) as src:
                return (
                    Path(fn).stem,
                    src.read(1, window=window).astype(np.float64)[mask],
                )
        except Exception as e:
            raise Exception(f'error when reading {fn}: {str(e)}')
    return wrapped

def parse_window(X, mask, ver):
    predictions = {}
    for target in selected_targets:

        _target = target
        if target not in non_logtrans:
            _target = 'ln_'+target

        for key in (
            target,
            target+'_q05',
            target+'_q95',
            target+'_qrange',
            _target,
            _target+'_q05',
            _target+'_q95',
            _target+'_qrange',
        ):
            if key not in predictions:
                predictions[key] = np.full_like(
                    mask,
                    OUTPUT_NODATA,
                    dtype=OUTPUT_DTYPE,
                )

        if mask.any():
            if _MODELS[target] is None:
                _MODELS[target] = pickle.load(open(fld_models_root/f'models{ver}/{target}/{target}.meta.pickle', 'rb'))

            (
                predictions[_target][mask],
                predictions[_target+'_q05'][mask],
                predictions[_target+'_q95'][mask],
            ) = map(
                lambda vals: vals.astype(OUTPUT_DTYPE),
                _MODELS[target].predict(X, return_interval=True)
            )

            predictions[_target+'_qrange'][mask] = predictions[_target+'_q95'][mask] - \
                predictions[_target+'_q05'][mask]

            if target not in non_logtrans:
                predictions[target][mask] = log_unpack(predictions[_target][mask])
                predictions[target+'_q05'][mask] = log_unpack(predictions[_target+'_q05'][mask])
                predictions[target+'_q95'][mask] = log_unpack(predictions[_target+'_q95'][mask])
                predictions[target+'_qrange'][mask] = predictions[target+'_q95'][mask] - \
                    predictions[target+'_q05'][mask]

    return predictions

def _get_tile_fn(target, i):
    return outdir.joinpath('tiles', target, f'{target}_{i+1}.tif')

def _tile_exists(i):
    return all((
        _get_tile_fn(target, i).exists()
        for target in selected_targets
    ))


def write_tile(i, window_dim, mask_negative=True):
    __, wtransform = make_window(i, window_dim)
    def wrapped(args):
        target, data = args
        if mask_negative:
            data[data<0] = OUTPUT_NODATA # sub-zero prediction artefacts
        outfile = _get_tile_fn(target, i)
        Path(outfile).parent.mkdir(parents=True, exist_ok=True)
        try:
            with rio.open(
                outfile, 'w',
                **{
                    **OUT_PROFILE,
                    **wtransform,
                }
            ) as dst:
                dst.write(data.astype(OUTPUT_DTYPE), 1)
        except rio.errors.RasterioIOError:
            os.makedirs(outfile.parent, exist_ok=True)
            wrapped(args)
    return wrapped

def write_to_mosaic(i, window_dim):
    window, __ = make_window(i, window_dim)
    def wrapped(args):
        target, data = args
        data[data<0] = OUTPUT_NODATA # sub-zero prediction artefacts
        outfile = outdir.joinpath(target+'.tif')

        if outfile.exists():
            mode = 'r+'
        else:
            mode = 'w'

        with rio.open(
            outfile, mode,
            **OUT_PROFILE,
        ) as dst:
            dst.write(data.astype(OUTPUT_DTYPE), 1, window=window)
    return wrapped


def do_predict():   
    pp = PreProcess(fn_models_pp)
    fn_layers = get_predictors_layers()
    layers = list(filter(lambda x: x.with_suffix('').name in pp.input_columns, fn_layers))  
    maskfile = fld_data_root.joinpath('clm_ece_mask.tif')

    n_tiles = raster_n_tiles(predict_window_dim)

    try:
        with ThreadPool(len(layers)) as io_pool:
            with rio.open(maskfile) as mask_src:
                for i in range(n_tiles):
                    if _tile_exists(i):
                        continue
                    # i=25;  window, __ = rutil.make_window(i, window_dim); mask = mask_src.read(1, window=window).astype(bool); print(mask.any())
                    window, __ = make_window(i, predict_window_dim)

                    ttprint(f'reading tile {i+1} of {n_tiles}')
                    mask = mask_src.read(1, window=window).astype(bool)

                    if mask.any():
                        X = pd.DataFrame(dict(io_pool.map(
                            read_layer(window, mask),
                            layers,
                        )))
                        
                        ttprint(f'tile {i+1}: preprocessing...')
                        X = pp.process(X)

                        ttprint(f'tile {i+1}: inferring...')                        
                        predictions = parse_window(X, mask, ver)
                        

                        ttprint(f'tile {i+1}: writing...')
                        if write_individual_tiles:
                            __ = io_pool.map(
                                write_tile(i, window_dim),
                                predictions.items(),
                            )
                        else:
                            __ = io_pool.map(
                                write_to_mosaic(i, window_dim),
                                predictions.items(),
                            )
                        
                        ttprint(f'tile {i+1}: done.')

                        del X, predictions 

                    del window, mask, __
                    gc.collect()

        ttprint(f'Done all.')

    except KeyboardInterrupt as e:
        raise e

    except MemoryError as e:
        ttprint(f'ERROR: {str(e)}\nrestarting...')
        os.execl(
            sys.executable,
            sys.executable,
            *sys.argv,
        )


if __name__ == '__main__':
    import sys
    
    target = sys.argv[1]
    if len(sys.argv)>2:
        ver = sys.argv[2]
        ver = f'_{ver}'
    else:
        ver=''
    
    if target== 'all':
        selected_targets=targets
    elif ',' in target:
        selected_targets = list(map(lambda x: x.strip(), target.split(',')))
    else:
        selected_targets=[target]      

    write_individual_tiles = True
    window_dim = 1024

    outdir = fld_data_predictions/f'predictions{ver}'

    _MODELS = {
        t: None
        for t in selected_targets
    }

    do_predict()

