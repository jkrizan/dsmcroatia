

# # Compilation of soil chemical and physical properties for Croatia
# 
# Part of: [World Bank funded project](https://gitlab.com/multione/wbsoilhr)  
# authors: Tom Hengl (tom.hengl@envirometrix.net), Josip Križan (jkrizan@multione.hr)
# 
# ## Martinović, J. and Vranković, A. (Editors), 1997. Baza podataka o hrvatskim tlima, I. Državna uprava za zaštitu prirode i okoliša, Zagreb, 365 pp.
# %%
import pandas
import geopandas
import numpy as np
from pathlib import Path
from settings import fld_data_inputs, fld_db

source_xlsx = fld_data_inputs/'DSMCroatia input MultiOne.xlsx'
source_hapih2019 = fld_data_inputs/'HAPIH_Ispitivanje plodnosti tla_2019.xlsx'
file_wrb_lookup = fld_data_inputs/'Martinovic_WRB_lookup.xlsx'
file_wrb2014_lookup = fld_data_inputs/'Martinovic_WRB2014_lookup.xlsx'
file_wrb2014_azo2016 = fld_data_inputs/'AZO2016_WRB2014_lookup.xlsx'


final_cols = ['source_db', 'source_sampled', 'site_key','site_obsdate', 'longitude_decimal_degrees', 'latitude_decimal_degrees', 'pedon_completeness_index', 'hzn_top','hzn_bot', 'oc', 'n_tot_ncs', 'ca_mehlich3', 'k_mehlich3', 
              'mg_mehlich3', 'p_mehlich3', 'cec_sum', 'ec_satp', 'caco3', 'ph_h2o', 'ph_kcl', 'clay_tot_psa', 'silt_tot_psa', 'sand_tot_psa', 'wpg2', 'db_od', 'dbr', 'wrb_rsg', 'wrb_qual1', 'wrb_qual2']


#%%
# ['source_db', 'source_sampled', 'site_key','site_obsdate', 'longitude_decimal_degrees', 'latitude_decimal_degrees', 'pedon_completeness_index', 'dbr']
print("Piezometri ...")
dfsrc1 = pandas.read_excel(source_xlsx, sheet_name='piez_monitoring_2015_2018')
dfsrc2 = pandas.read_excel(source_xlsx, sheet_name='piez_hidroloski_monitoring')

#%%
ind = (dfsrc1['vrsta_objekta'].str.strip()=='piezometar') & (~dfsrc1.x.isnull()) & (~dfsrc1.y.isnull())
dfw1 = dfsrc1.loc[ind,:].copy()
dfw1['site_key'] = dfw1.redni_broj.apply(lambda x: f'Piez1_{x}')
#dfw1['longitude_decimal_degrees'] = dfw1.x
#dfw1['latitude_decimal_degrees'] = dfw1.y

ind = (~dfsrc2.x.isnull()) & (~dfsrc2.y.isnull())
dfw2 = dfsrc2.loc[ind,:].copy()
dfw2['site_key'] = dfw2.redni_broj.apply(lambda x: f'Piez2_{x}')
#dfw1['longitude_decimal_degrees'] = dfw1.x
#dfw1['latitude_decimal_degrees'] = dfw1.y

dfw = dfw1.loc[:, ['site_key','x','y']].append(dfw2.loc[:, ['site_key','x','y']])

dfg = geopandas.GeoDataFrame(dfw.site_key,geometry=geopandas.points_from_xy(dfw['x'].values, dfw['y'].values),crs="EPSG:3765")
dfg = dfg.to_crs('EPSG:4326')
dfg['longitude_decimal_degrees'] = dfg.geometry.x
dfg['latitude_decimal_degrees'] = dfg.geometry.y
del dfg['geometry']

dfj = pandas.merge(dfw, dfg, on='site_key', how='left')

dfj['source_db'] = 'piezometri'
dfj['source_sampled'] = 'piezometri'
dfj['confidence_degree'] = 1
dfj['pedon_completeness_index']=0
dfj['site_obsdate'] = None

dfj['dbr'] =400

df_bpht =  dfj.loc[:, dfj.columns.intersection(final_cols)]
df_bpht.to_pickle(fld_db/'tmp/df_piezometri.pickle')



# %%
# Read input data
print("Martinović1997 ...")
dfsrc = pandas.read_excel(source_xlsx, sheet_name='Martinovic1997')

# %%
# remove empty rows:
dfw = dfsrc.dropna(subset=['PEDOL_ID']) 
dfw['x'], dfw['y'] = dfw['Cro16,30_X'], dfw['Cro16,30_Y']
dfw.loc[~dfw.corrX.isna(),['x','y']] = dfw.loc[~dfw.corrX.isna(),['corrX','corrY']].values

dfw.PEDOL_ID = dfw.PEDOL_ID.astype(int)

# add site_key
dfw['site_key'] = dfw.PEDOL_ID.apply(lambda x: f'Martinovic1997_{x}')

# taxgrtgroup
dfl = pandas.read_excel(file_wrb_lookup)[['PEDOL_ID','taxgrtgroup']].set_index('PEDOL_ID')
dfw = dfw.join(dfl,how='left',on='PEDOL_ID')
dfl = pandas.read_excel(file_wrb2014_lookup).set_index('wrb_old').replace(np.nan,'')
dfw = dfw.join(dfl, how='left', on='taxgrtgroup')

# get horizons
hor_cols = ["OZN", "GOR", "DON", "TT", "OSA", "IS",  "MKP", "PH1", "PH2", "MSP", "MP", "MG", "HUM", "EXTN", "EXTP", "EXTK", "CAR"]
id_col = 'site_key'
dfHors = pandas.DataFrame(columns=[id_col]+hor_cols)
for hor in range(1,10):
    cols = list(map(lambda c: f'HOR{hor}_{c}', hor_cols))
    dff = dfw.loc[:,[id_col]+cols]
    dff.columns = [id_col]+hor_cols
    dfHors = dfHors.append(dff)

for col in ('GOR','DON','MKP','MSP','MP','PH1','PH2','HUM','EXTN','EXTP','EXTK','CAR'):
    dfHors[col] = pandas.to_numeric(dfHors[col],errors='coerce')

dfHors.loc[dfHors.DON.isna(),'DON'] = dfHors.loc[dfHors.DON.isna(),'GOR']+50
dfHors['hzn_top'] = dfHors.GOR
dfHors['hzn_bot'] = dfHors.DON

# drop rows without depths
dfHors = dfHors.dropna(subset=['hzn_top','hzn_bot'])

# some calculations
# oc
dfHors['oc'] = (dfHors.HUM/1.724)
# sand_tot_psa
dfHors['sand_tot_psa'] = dfHors.MSP*0.8 + dfHors.MKP
#   correction
ind = (dfHors.site_key=='Martinović1997_805') & (dfHors.OZN=='Amo')
dfHors.loc[ind, 'sand_tot_psa'] = dfHors.sand_tot_psa / 10.0

# silt_tot_psa
dfHors['silt_tot_psa'] = dfHors.MP + dfHors.MSP*0.2

# clay_tot_psa
dfHors['clay_tot_psa'] = dfHors.MG

# caco3
dfHors['caco3'] = dfHors['CAR']

# ph
dfHors['ph_h2o'] = dfHors['PH1']
dfHors['ph_kcl'] = dfHors['PH2']

# join original with horizontal data
dfw = dfw[["site_key", "PEDOL_ID", "UZORAK", "x", "y", "FITOC", "STIJENA", "TLO_TIP", "PODTIP", "HID_DREN", "DUBINA", "FAO_NAME",'wrb_rsg','wrb_qual1']]
dfj = dfw.join(dfHors.set_index('site_key'), 'site_key')

# wpg2
dfj['wpg2'] = dfj['STIJENA']
ind = dfj.GOR<30
dfj.loc[ind,'wpg2'] = dfj.loc[ind,'wpg2']*0.3

# convert N, P, K
dfj['p_mehlich3'] = dfj.EXTP * 4.364
dfj['k_mehlich3'] = dfj.EXTK * 8.3013
dfj['n_tot_ncs'] = dfj.EXTN 

# Depth to bedrock, first way
'''
print('Finding depth to bedrock')
patterns=["^R", "/R", "CR"]
patterns_ref=['GOR', 'DON', 'DON']
patterns_type=[r'\blitic'] # u tip_tla i u podtip samo početak riječi

df=pandas.DataFrame()
for pat, pat_ref in zip(patterns,patterns_ref):
    #pat, pat_ref = ['/R', 'DON'] #next(zip(patterns,patterns_ref)) 
    ind = dfj.OZN.str.contains(pat, case=True, na=False)
    print(f'Horizon cases with "{pat}" in "OZN": {ind.sum()}')
    if ind.any():
        dff = dfj.loc[ind, ['site_key', pat_ref]]
        dff.columns=['site_key','dbr']
        df = df.append(dff)
#print(df)

for pat in patterns_type:
    ind = (dfj.TLO_TIP+' '+dfj.PODTIP).str.contains(pat, case=False, na=False)
    print(f'Cases with "{pat}" in "TLO_TIP, PODTIP": {ind.sum()}')
    if ind.any():
        dff = dfj.loc[ind, ['site_key', 'DON']]
        dff.columns = ['site_key','dbr1']
        df = df.append(dff)

df = df.groupby('site_key').agg('max')
print(f'Cases with depth to bedrock: {len(df)}')
dfj = dfj.join(df, 'site_key')
'''

#  'DUBINA' from original data is also depth to bedrock!
dfj['dbr'] = dfj['DUBINA']
dfj.loc[dfj.dbr==0,'dbr'] = np.nan
# some corrections of wrong input data
'''
dfj.loc[dfj.site_key=='Martinovic1997_1607','dbr'] = 200 #np.nan #Područje kod Novske
dfj.loc[dfj.site_key=='Martinovic1997_1712','dbr'] = 200 #Područje kod Novske
dfj.loc[dfj.site_key=='Martinovic1997_1713','dbr'] = 200 #np.nan #Područje kod Novske
dfj.loc[dfj.site_key=='Martinovic1997_1714','dbr'] = 200 #np.nan #Područje kod Novske
dfj.loc[dfj.site_key=='Martinovic1997_1715','dbr'] = 200 #np.nan #Područje kod Novske
'''
dfj.loc[dfj.site_key=='Martinovic1997_1607','dbr'] = np.nan
dfj.loc[dfj.site_key=='Martinovic1997_2173','dbr'] = np.nan
dfj.loc[dfj.site_key=='Martinovic1997_1772','dbr'] = np.nan
dfj.loc[dfj.site_key=='Martinovic1997_835','dbr'] = np.nan
dfj.loc[dfj.site_key=='Martinovic1997_1781','dbr'] = np.nan
dfj.loc[dfj.site_key=='Martinovic1997_1888','dbr'] = np.nan


# Coordinates
crs_1630 = "+proj=tmerc +lat_0=0 +lon_0=16.5 +k=0.9997 +x_0=2500000 +y_0=0 +ellps=bessel +towgs84=426.9,142.6,460.1,4.91,4.49,-12.42,17.1 +units=m +no_defs"
dfg = geopandas.GeoDataFrame(dfw.site_key,geometry=geopandas.points_from_xy(dfw['x'].values, dfw['y'].values),crs=crs_1630)
dfg = dfg.to_crs('EPSG:4326')
dfg['longitude_decimal_degrees'] = dfg.geometry.x
dfg['latitude_decimal_degrees'] = dfg.geometry.y
del dfg['geometry']

dfj = pandas.merge(dfj, dfg, on='site_key', how='left')

dfj['source_db'] = 'martinovic_1997'
dfj['source_sampled'] = 'martinovic'
dfj['confidence_degree'] = 5
dfj['pedon_completeness_index']=100
dfj['site_obsdate'] = dfj.UZORAK.apply(lambda x: '' if np.isnan(x) else f'{x:.0f}')

df_bpht =  dfj.loc[:, dfj.columns.intersection(final_cols)]

print()

df_bpht.to_pickle(fld_db/'tmp/df_martinovic_1997.pickle')

#%%
# ## Spatial variability of trace and toxic metals in agricultural soils of Croatia  
# ### Project Leader: Marija Romić  
# Summary:  
# The problem of exposure of agricultural soils to different anthropogenic inputs of toxic metals, but also of other potentially toxic substances, has acquired global dimensions in the last decades. Besides atmospheric deposition, 
# environmental dispersion of chemicals used in agriculture is an important factor directly affecting the natural soil functions, or indirectly endangering the biosphere by bioaccumulation and inclusion into the food chain. Metal 
# concentrations in soil can be generally predicted started from the element abundance in the parent material. The extent to which pedogenesis affect heavy metals distribution varies according to the prevailing factors affecting soil 
# processes. Because of the toxicity to plants and animals, it is important to determine their content, forms and distribution. Such hypothesis may be tested by total metal content determination, as well as other elements relevant 
# for geochemical valorization of the agricultural soils of Croatia. Thus, the spatial variability and baseline of elements in soils will be determined by means of relevant statistical and geostatistical methods. The maps of toxic 
# metal distribution will be produced and the suitability of soils for agriculture will be assessed. GIS is increasingly used in environmental assessment studies because of its ability to superimpose different spatial information and 
# to combine them with the results of statistical analysis, enabling thus the detection of complex spatial relationships among different parameters. Geostatistics and multivariate statistics has been widely used in geochemical studies 
# to identify pollution sources and to apportion natural vs. anthropogenic contribution, establishing as well a geochemical background. The main objectives of the investigation are: (i) to provide geochemical database relevant to the 
# agricultural soils in Croatia; (ii) to provide a detailed information about the natural variability of the geochemical background which is pertinent to administrative and legal issues as well as to safety food production and environmental 
# protection; (iii) presenting the influence of human and other environmental activities on the soil quality mainly regarding the toxic and trace metal contents, and (iv) we are going to observe the influence of natural conditions on 
# regional differences which have been widely neglected so far, and have not been taken into account while national regulations and guidelines on soil toxic metal contents have been established. 

# %%
print('agricultural_2013')
dfsrc_agr = pandas.read_excel(source_xlsx, sheet_name='ROMIC_8x8')

# %%
dfw = dfsrc_agr.copy()
dfw['source_db'] = 'agricultural_2013'
dfw['source_sampled'] = 'agriculture'
dfw['confidence_degree'] = 1
dfw['pedon_completeness_index'] = 50

dfw['site_key'] = dfw['Sample ID'].apply(lambda x: f'AGR_{x}')
dfw['site_obsdate'] = dfw['year']

dfw['oc'] = dfw['Corg_g/kg'] / 10
dfw[['hzn_top','hzn_bot']] = pandas.DataFrame(dfw.Depth.str.split('-').tolist(), index=dfw.index)
dfw['p_mehlich3'] = dfw['P2O5_mg/100g'] * 4.364
dfw['k_mehlich3'] = dfw['K2O_mg/100g'] * 8.3013

dfw = dfw.rename(columns={'ph_H2O':'ph_h2o', 'pH_KCL':'ph_kcl', 'CaCO3_%':'caco3', 'EC_dS/m':'ec_satp', 'Sand':'sand_tot_psa', 'Clay':'clay_tot_psa', 'Silt':'silt_tot_psa', 'lat':'latitude_decimal_degrees', 'lon':'longitude_decimal_degrees'})

df_agr = dfw.loc[:,dfw.columns.intersection(final_cols)]

df_agr.to_pickle(fld_db/'tmp/df_agricultural_2013.pickle')
#df_agr.info()

#%%
# ## Promjena zaliha ugljika u tlu i izračun trendova ukupnog dušika i organskog ugljika u tlu te odnosa C:N - WFS  
# 
# ### Miko, S., Hasan, O., Komesarović, B., Ilijanović, N., ŠparicaMiko, M., Đumbir, A.M., Ostrogović Sever, M.Z., Paladinić, E., Marjanović, H., 2017. : ["Promjena zaliha ugljika u tlu i izračun trendova ukupnog dušika i organskog ugljika u tlu 
# te odnosa C:N – Knjiga I: Izvješće o setu podataka za izradu izvješća prema Okvirnoj konvenciji UN-a o promjeni klime – UNFCCC (sektori LULUCF i poljoprivreda)"](http://envi.azo.hr/?topic=3). Zagreb: Hrvatski geološki institut, 81 p.
# 
# Projekt je financiran sredstvima Fonda za zaštitu okoliša i energetsku učinkovitost u okviru Programa „Dogradnja i razvoj Informacijskog sustava zaštite okoliša i unapređenje sustava praćenja i izvješćivanja o stanju okoliša u Republici Hrvatskoj“, 
# komponenta 2.: Unapređenje sustava praćenja i izvješćivanja o stanju okoliša u Republici Hrvatskoj; unapređenje sustava prikupljanja i razmjene podataka te izrada metodologija za njihovu obradu u skladu sa smjernicama UNFCCC i Kyotskog protokola 
# definirane IPCC (Intergovernmental Panel on Climate Change).  
# 
# Nositelj projekta je Ministarstvo zaštite okoliša i energetike, a izvršitelji Hrvatski geološki institut, Hrvatski šumarski institut i Agencija za poljoprivredno zemljište.  
# 
# U razdoblju 2014. – 2017. godina, na 725 reprezentativnih lokacija provedeno je terensko i laboratorijsko istraživanje stanja tla. Prikupljeni su opći podaci o lokaciji uzorkovanja koji sadrže administrativne, lokacijske, geografske i ostale 
# podatke (reljef, klimatske i meteorološke podatke, detaljne podatke o korištenju zemljišta i biljnom pokrovu, opis površinskih svojstava tla).  
# 
# Terensko uzorkovanje tla za svaku LULUCF kategoriju korištenja zemljišta provedeno je prema modificiranoj metodologiji opisanoj u EU DG JRC (Joint research centre) „Protokolu za uzorkovanje tla radi potvrđivanja promjena zaliha organskog ugljika u EU“ 
# autora Stolbovoy i dr. 2007 (Soil sampling protocol to certify the changes of organic carbon stock in mineral soil of the European Union – EU JRC). Modifikacije protokola imale su za cilj osigurati izvješćivanje po UNFCCC i Kyoto protokolu, 
# odnosno osigurati sukladnost s IPCC metodologijom. Uzorkovanje tala na šumskom zemljištu (FL) prema protokolu JRC-a predviđeno je na dvije dubine 0-10 cm i 10 – 20 cm i organski sloj (listinac), ali je radi zahtjeva izvješćivanja po UNFCCC i 
# Kyoto protokolu provedeno uzorkovanje i na dubini 20 – 30 cm. Zemljište pod usjevima (CL)uzorkovano je na dvije dubine (0-20 cm i 20-30 cm) a travnjaci (GL), močvare (WL), naselja (SL) i ostalo zemljište (OL) uzorkovani su na tri dubine 0-10, 
# 10-20 i 20-30 cm.  
# Geokemijske analize obavljene su na dubinama 0-10 i 20-30 cm za šumska zemljišta (FL) te za livade i pašnjake (GL) dok su za zemljište pod usjevima (CL) analizirani kompozitni uzorci 0-30 cm i 0-20 cm.

# %%
print('azo_2013 ...')
dfsrc_azo_2013 = pandas.read_excel(source_xlsx, sheet_name='zalihe_c_n_u_tlu_2013_0_25')

# %%
dfw = dfsrc_azo_2013.copy()
dfw['source_db'] = 'azo_2013'
dfw['source_sampled'] = 'hgi2013'
dfw['pedon_completeness_index'] = 50
dfw['confidence_degree'] = 2
dfw['site_key'] = dfw['tocka'].apply(lambda x: f'AZO2013_{x}')

dfw['site_obsdate'] = np.nan # TODO: read from AZO_2013_dates.xlsx

crs_GK5 = '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=550.499,164.116,475.142,5.80967,2.07902,-11.62386,5.541764 +units=m +no_defs'
dfg = geopandas.GeoDataFrame(dfw.site_key,geometry=geopandas.points_from_xy(dfw['y5'], dfw['x5']),crs=crs_GK5)
dfg = dfg.to_crs('EPSG:4326')
dfg['longitude_decimal_degrees'] = dfg.geometry.x
dfg['latitude_decimal_degrees'] = dfg.geometry.y
dfw = dfw.join(dfg[['site_key', 'longitude_decimal_degrees', 'latitude_decimal_degrees']].set_index('site_key'), on='site_key')

# dfw['oc'] = dfw['soc___corr'] # Ovo je pogrešna kolona, organski ugljik je bio računan a ne mjeren

# n_tot_ncs ... n__, n__ponovlj, 
dfw['n_tot_ncs'] = dfw.apply(lambda x: float(x['n__ponovlj'].replace(',','.')) if isinstance(x['n__ponovlj'],str) else x['n__'], axis=1)

#ca_mehlich3
#k_mehlich3
#mg_mehlich3
#p_mehlich3
#cec_sum
#ec_satp
#caco3
#ph_h2o
#ph_kcl
#clay_tot_psa
#silt_tot_psa
#sand_tot_psa
#wpg2, [kamenit] is from pedological map, we will not use it
#dfw['wpg2'] = dfw.kamenit.str.split('-').apply(lambda x: None if repr(x)=='nan' else int(x[0]) if len(x)==1 else 0.5*(int(x[0])+int(x[1])))
#db_od
dfw['db_od'] = np.nanmean(dfw[['soil_densi', 'soil_den_1', 'soil_den_2', 'soil_den_3', 'soil_den_4']].values, axis=1)
#dbr
#taxgrtgroup
dfl = pandas.read_excel(file_wrb2014_azo2016).set_index('tip_tla').replace(np.nan, '')
dfw = dfw.join(dfl, on='tip_tla')

dfw[['hzn_top','hzn_bot']] = pandas.DataFrame(dfw.dubina.str.split('-').to_list(),index=dfw.index).astype(int)

df_azo2103 = dfw.loc[:,dfw.columns.intersection(final_cols)]

df_azo2103.to_pickle(fld_db/'tmp/df_azo2013.pickle')

#%%
# ### Data from 'c_n_2016_u_tlu_0_30'. Data are collected on 725 chosen location between 2014 and 2017

# %%
print('azo_2016 ...')
dfsrc_azo_2016 = pandas.read_excel(source_xlsx, sheet_name='promjena_zaliha_c_n_u_tlu')
# this data is in form of three horizons (0-10, 10-20, 20-30 cm)
# Most of points have 0-10, 10-20 and 20-30 depth, but not all
# Instead of 'dubina' we will use 'nazivna_du' 

# %%
dfw = dfsrc_azo_2016.copy()
dfw.columns = map(lambda x: x.lower(), dfw.columns)
dfw['dubina'] = dfw['nazivna_du']

dfw['source_db'] = 'azo_2016'
dfw['source_sampled'] = dfw['uzorkovao']
dfw['pedon_completeness_index'] = 50
dfw['confidence_degree'] = 1
dfw['site_key'] = dfw['tocka_id'].apply(lambda x: f'AZO2016_{x}')
dfw['site_obsdate'] = dfw['datum_2_uz'].dt.year # TODO: there is month also in [datum_uzor]

crs_GK5 = '+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=5500000 +y_0=0 +ellps=bessel +towgs84=550.499,164.116,475.142,5.80967,2.07902,-11.62386,5.541764 +units=m +no_defs'
dfg = geopandas.GeoDataFrame(dfw.site_key,geometry=geopandas.points_from_xy(dfw['y5'], dfw['x5']),crs=crs_GK5)
dfg = dfg.to_crs('EPSG:4326')
dfg['longitude_decimal_degrees'] = dfg.geometry.x
dfg['latitude_decimal_degrees'] = dfg.geometry.y

dfw = dfw.join(dfg[['site_key', 'longitude_decimal_degrees', 'latitude_decimal_degrees']].drop_duplicates().set_index('site_key'), on='site_key')

# oc 
dfw['oc'] = dfw['org_c_2016'] 

# n_tot_ncs ... n__, n__ponovlj, 
dfw['n_tot_ncs'] = dfw['n_2016']

#ca_mehlich3
dfw['ca_mehlich3'] = dfw['ca_mg_100g'] * 10
#k_mehlich3
dfw['k_mehlich3'] = dfw['k_mg_100g'] * 10
#mg_mehlich3
dfw['mg_mehlich3'] = dfw['mg_mg_100g'] * 10
#p_mehlich3 #dfw['p'] ? 

#cec_sum
#ec_satp
#caco3
    # already in 'caco3' column
#ph_h2o
    # already in 'ph_h2o' columns
#ph_kcl
    # there is ph_cacl column? No, this is not the same.
#clay_tot_psa
dfw['clay_tot_psa'] = dfw['glina_ispo']
#silt_tot_psa
dfw['silt_tot_psa'] = dfw.sitni_prah + dfw.krupni_pra*0.2 + 0.5*(dfw.krupni_pra + dfw.ukupno_pra)
#sand_tot_psa
dfw['sand_tot_psa'] = dfw.krupni_pra*0.8 + dfw.pijesak__d
#wpg2   # 'kamenitost','kamenito_1','stjenovito' # TODO: Ask if we can use some of this
#dfw['wpg2'] = dfw.kamenito_1.str.split('-').apply(lambda x: None if repr(x)=='nan' else int(x[0]) if len(x)==1 else 0.5*(int(x[0])+int(x[1])))

#db_od
dfw['db_od'] = dfw['volumna_gu'] #np.nanmean(dfw[['soil_densi', 'soil_den_1', 'soil_den_2', 'soil_den_3', 'soil_den_4']].values, axis=1)
#dbr
#  No data for depth to bedrock

#taxgrtgroup
#dfw['taxgrtgroup'] = dfw['tip_tla']
dfl = pandas.read_excel(file_wrb2014_azo2016).set_index('tip_tla').replace(np.nan, '')
dfw = dfw.join(dfl, on='tip_tla')

dfw[['hzn_top','hzn_bot']] = pandas.DataFrame(dfw.dubina.str.split('-').to_list(),index=dfw.index).astype(int)

df_azo2016 = dfw.loc[:,dfw.columns.intersection(final_cols)]

df_azo2016.to_pickle(fld_db/'tmp/df_azo2016.pickle')

#%% 
# ## HAPIH data for 2019

# %%
print('HAPIH 2019 ...')
df_hapih = pandas.read_excel(source_hapih2019)

# %%
def clean_data(data: pandas.Series):
    clean = data.astype(str).str.replace(',','.',regex=False).str.replace('*','nan',regex=False) #.astype(float)
    clean = clean.str.replace('ND', 'nan')
    clean[clean.str.contains('[<>]')] = 'nan'
    return clean.astype(float)

dfw = df_hapih[['rb','godina','lat','lon','datum_uzorkovanja','dubina_uzorkovanja','ph_kcl','ph_h2o','humus','p2o5_al','k2o_al','ukupni_n','caco3','ovlasteni_lab']].copy()

dfw['source_db'] = 'hapih2019'
dfw['site_key'] = dfw.rb.apply(lambda x: f'hapih_{x}')
dfw['source_sampled'] = dfw['ovlasteni_lab']
dfw['site_obsdate'] = 2019 # 'datum_uzorkovanja' has date but in different formats ...
dfw['pedon_completeness_index'] = 50

#coordinates
dfw['longitude_decimal_degrees'] = clean_data(dfw.lon) #.astype(str).str.replace(',', '.').astype(float)
dfw['latitude_decimal_degrees'] = clean_data(dfw.lat) #.astype(str).str.replace(',', '.').astype(float)
valid_ind = (~dfw.longitude_decimal_degrees.isnull()) & (dfw.longitude_decimal_degrees>13) & (dfw.longitude_decimal_degrees<20)
valid_ind = valid_ind & (~dfw.latitude_decimal_degrees.isnull()) & (dfw.latitude_decimal_degrees>42) & (dfw.latitude_decimal_degrees<46.6)

#depth
depth = dfw.dubina_uzorkovanja.str.replace('cm','').str.replace(' ','').str.replace('o','0')
ind = ~depth.isnull()
dd = pandas.DataFrame(depth[ind].str.split('-').tolist(), index=dfw.index[ind])
dfw['hzn_top'] = -1; dfw['hzn_bot'] = -1
dfw.loc[ind, 'hzn_top'] = dd.loc[ind,0].astype(int)
dfw.loc[ind, 'hzn_bot'] = dd.loc[ind,1].astype(int)
valid_ind = valid_ind & (dfw.hzn_top>=0) & (dfw.hzn_bot>0)

#
dfw['ph_kcl'] = clean_data(dfw.ph_kcl) #.astype(str).str.replace(',','.').str.replace('*','nan').astype(float)
dfw['ph_h2o'] = clean_data(dfw.ph_h2o) #.astype(str).str.replace(',','.').str.replace('*','nan').astype(float)
dfw['oc'] = clean_data(dfw.humus) #.str.replace(',','.').str.replace('*','nan').astype(float) / 1.724
dfw['p_mehlich3'] = clean_data(dfw.p2o5_al) * 4.364 #.str.replace(',','.').str.replace('*','nan').str.replace('ND','nan').astype(float)  * 4.364
dfw['k_mehlich3'] =  clean_data(dfw.k2o_al) * 8.3013 #.str.replace(',','.').str.replace('*','nan').str.replace('ND','nan').astype(float)  * 8.3013
dfw['n_tot_ncs'] = clean_data(dfw.ukupni_n)
dfw['caco3'] = clean_data(dfw.caco3)

df_hapih2019 = dfw.loc[valid_ind,dfw.columns.intersection(final_cols)]

df_hapih2019.to_pickle(fld_db/'tmp/df_hapih2019.pickle')

#%%
# ## Binding of all sources
print('Binding all sources ...')
df_bpht = pandas.read_pickle(fld_db/'tmp/df_martinovic_1997.pickle')
df_agr = pandas.read_pickle(fld_db/'tmp/df_agricultural_2013.pickle')
df_azo2013 = pandas.read_pickle(fld_db/'tmp/df_azo2013.pickle')
df_azo2016 = pandas.read_pickle(fld_db/'tmp/df_azo2016.pickle')
df_hapih2019 = pandas.read_pickle(fld_db/'tmp/df_hapih2019.pickle')
df_piez = pandas.read_pickle(fld_db/'tmp/df_piezometri.pickle')

df_all = pandas.concat([df_bpht, df_agr, df_azo2013, df_azo2016, df_hapih2019, df_piez])
df_all.to_pickle(fld_db/'tmp/df_binded_hors.pickle')

#%% ### Extracting of top soil data (0-30 cm)

df_all = pandas.read_pickle(fld_db/'tmp/df_binded_hors.pickle')
profile_cols = ['source_db', 'source_sampled', 'site_key', 'site_obsdate', 'longitude_decimal_degrees', 'latitude_decimal_degrees', 'pedon_completeness_index','dbr']
numeric_cols = ['oc', 'n_tot_ncs', 'ca_mehlich3', 'k_mehlich3', 'mg_mehlich3', 'p_mehlich3', 'cec_sum', 'ec_satp', 'caco3', 'ph_h2o', 'ph_kcl', 'clay_tot_psa', 'silt_tot_psa','sand_tot_psa', 'wpg2', 'db_od']
categorical_cols = ['wrb_rsg', 'wrb_qual1', 'wrb_qual2']
hors_cols = ['hzn_top', 'hzn_bot'] + numeric_cols + categorical_cols

# Check uniqueness of profile_cols values
print('Check uniqueness of profile_cols values:')
df_profile = df_all[profile_cols+categorical_cols].drop_duplicates()
dfc = df_profile.groupby('site_key').count()
min_count, max_count = dfc.min(axis=1).min(), dfc.max(axis=1).max()
if min_count<=max_count==1:
    print('OK')
else:
    print('ERROR, check profile_cols!')
    inds = dfc[dfc.max(axis=1)>1].index
    print(f"There is {len(inds.unique())} site_key's with not equal profile data")
    #   Checking coordinates
    dfc = df_profile[['site_key','latitude_decimal_degrees','longitude_decimal_degrees']].drop_duplicates().groupby('site_key').count()
    bad_site_keys = dfc.index[dfc.max(axis=1)>1]
    if len(bad_site_keys) > 0:
        print(f'There is {len(bad_site_keys)} profiles with different coordinates for horizons: {bad_site_keys}')
        print('These profiles will be ignored.')
        df_profile = df_profile.loc[~df_profile.site_key.isin(bad_site_keys)]
    
    # Checking source_sampled
    dfc = df_profile[['site_key','source_sampled']].drop_duplicates().groupby('site_key').count()
    bad_site_keys = dfc.index[dfc.max(axis=1)>1]
    print(f'There is {len(bad_site_keys)} profiles with different [source_sampled] data for horizons: {bad_site_keys}')
    print('This is not so important so we will just take first one.')
    dfc = df_profile[['site_key','source_sampled']].drop_duplicates().groupby('site_key').count()
    bad_site_keys = dfc.index[dfc.max(axis=1)>1]
    inds = df_profile.site_key.isin(bad_site_keys)
    dfl = df_profile.groupby('site_key').first()['source_sampled'][bad_site_keys]
    df_profile.source_sampled.loc[inds] = df_profile.site_key[inds].map(dfl)

    # One more check
    dfc = df_profile.drop_duplicates().groupby('site_key').count()
    min_count, max_count = dfc.min(axis=1).min(), dfc.max(axis=1).max()
    if max_count>1:
        print('There is still some errors in df_profile !')

    #print(df_profile[df_profile.site_key.isin(inds)].__repr__())
print()


df_hors = df_all[df_all.columns.intersection(['site_key'] + hors_cols)].set_index('site_key')
# Calculate top soil properties
# 
df_hors['hzn_top'] = df_hors.hzn_top.apply(float)
df_hors['hzn_bot'] = df_hors.hzn_bot.apply(float)

#   check hzn_top < hzn_bot
ind = (df_hors.hzn_top>=df_hors.hzn_bot) | (df_hors.hzn_top<0)
print('Check hzn_top < hzn_bot:')
if ind.any():
    print('ERROR: some hzn_top>=hzn_bot')
else:
    print('OK')
print()

# dbr fixing
## calculate maximum horizon depth
df_mhd = df_hors.groupby('site_key').agg({'hzn_bot':'max'}).rename(columns={'hzn_bot':'max_hzn_depth'})
df_profile = df_profile.join(df_mhd, on='site_key')
## max horizon depth > dbr  =>  dbr = max horizon depth
ind = (df_profile.source_db=='martinovic_1997') & (df_profile.max_hzn_depth> df_profile.dbr)
df_profile.loc[ind,'dbr'] = df_profile.max_hzn_depth[ind]
## dbr is unknown and max_hor_depth >= 100
ind = (df_profile.source_db=='martinovic_1997') & pandas.isnull(df_profile.dbr) & (df_profile.max_hzn_depth>=100)
df_profile.loc[ind, 'dbr'] = df_profile.max_hzn_depth[ind]    #200
del df_profile['max_hzn_depth']

#    calculate weight of horizon thats equal of length of this horizon in 0-30 depth
df_hors['weight'] = (df_hors.hzn_bot.clip(0,30) - df_hors.hzn_top.clip(0,30))
# calculate weighted average 
dfw = df_hors[df_hors.columns.intersection(numeric_cols)].multiply(df_hors['weight'], axis='index').groupby(level=0).sum()
dfs = (~df_hors[df_hors.columns.intersection(numeric_cols)].isna()).astype(int).multiply(df_hors.weight, axis='index').groupby(level=0).sum()
df_props = dfw.divide(dfs, axis='index')

# Bind together profile variables and numerical
df_final = df_profile.set_index('site_key').join(df_props)
df_final.site_obsdate = df_final.site_obsdate.astype(float)
df_final.loc[df_final.site_obsdate==0, 'site_obsdate'] = np.nan

# cleaning extreme values
# OC over 20 at Martinović data
df_final.loc[df_final.query('oc>60').index,'oc'] = np.nan 
df_final.loc[df_final.query('n_tot_ncs>2').index,'n_tot_ncs'] = np.nan
df_final.loc[df_final.query('k_mehlich3>1000').index,'k_mehlich3'] = np.nan
df_final.loc[df_final.query('p_mehlich3>1000').index,'p_mehlich3'] = np.nan
df_final.loc[df_final.query('ec_satp>300').index,'ec_satp'] = np.nan
df_final.loc[df_final.query('caco3>70').index,'caco3'] = np.nan
df_final.loc[df_final.query('ph_h2o==0').index,'ph_h2o'] = np.nan
df_final.loc[df_final.query('sand_tot_psa>100').index,'sand_tot_psa'] = np.nan
df_final.loc[df_final.query('silt_tot_psa>100').index,'silt_tot_psa'] = np.nan
df_final.loc[df_final.query('clay_tot_psa>100').index,'clay_tot_psa'] = np.nan
df_final.loc[df_final.query('db_od==0').index,'db_od'] = np.nan

# WRB classification
df_final[['wrb_rsg','wrb_qual1','wrb_qual2']] = df_final[['wrb_rsg','wrb_qual1','wrb_qual2']].replace(np.NaN, '')
df_final['wrb_rsg']=df_final.wrb_rsg.apply(lambda x: f'{x}s' if x!='' and x[-1]!='s' else x)

# Calculating cec_sum
# ((clay_tot_psa*10/100*(30+4.4*ph_h2o))+(oc/100*(-59+51*ph_h2o)))/10  
#ind = df_final[['clay_tot_psa', 'ph_h20', 'oc']].notna().all(axis=1).values
#df_final['cec_sum'] = np.nan
df_final['cec_sum'] = ((df_final.clay_tot_psa*10/100*(30+4.4*df_final.ph_h2o))+(df_final.oc/100*(-59+51*df_final.ph_h2o)))/10
# Saving
df_final.to_pickle(fld_db/'tmp/hr_topsoil_db.pickle')
df_final.to_csv(fld_db/'hr_topsoil_db.csv')
df_final.to_excel(fld_db/'hr_topsoil_db.xlsx')

dfg = geopandas.GeoDataFrame(df_final, geometry=geopandas.points_from_xy(df_final.longitude_decimal_degrees.values, df_final.latitude_decimal_degrees.values), crs='EPSG:4326')

dfg.to_file(fld_db/'hr_topsoil_db.gpkg', layer='hr_topsoil_db', driver="GPKG")

print('All done.')




