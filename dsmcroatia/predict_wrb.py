import numpy as np
import rasterio as rio
from multiprocessing.pool import ThreadPool
import multiprocessing as mp
from operator import add
from functools import reduce
import xgboost as xgb
import pickle
import os, sys, re
import pandas as pd
from pathlib import Path
import gc

from utils import PreProcess, raster_n_tiles, make_window, ttprint, raster_get_profile, log_unpack
from settings import fld_models_root, fn_models_pp, fld_data_predictions, fld_data_root
from settings import targets, predict_window_dim, non_logtrans, get_predictors_layers

target_lvl0 = 'wrb_rsg'
target_lvl1 = 'wrb_qual1'
target_lvl2 = 'wrb_qual2'

# fixes os.exec* import errors...
_working_dir = Path(__file__).parent.parent.resolve().as_posix()
sys.path.append(_working_dir)

OUTPUT_NODATA = 255
OUTPUT_DTYPE = np.uint8

OUT_PROFILE = raster_get_profile(
    nodata=OUTPUT_NODATA,
    dtype=OUTPUT_DTYPE,
)

def _get_tile_fn(target, i):
    return outdir.joinpath('tiles', target, f'{target}_{i+1}.tif')

def _tile_exists(i):
    return _get_tile_fn(target_lvl0, i).exists()

def read_layer(window, mask):
    def wrapped(fn):
        try:
            with rio.open(fn) as src:
                return (
                    Path(fn).stem,
                    src.read(1, window=window).astype(np.float64)[mask],
                )
        except Exception as e:
            raise Exception(f'error when reading {fn}: {str(e)}')
    return wrapped

def parse_window(X, mask, encoder, model):
    target = target_lvl0

    predictions = {
        target: np.full_like(
            mask,
            OUTPUT_NODATA,
            dtype=OUTPUT_DTYPE,
        )
    }

    _proba_names = [
        f'{target}_proba_{cls.replace(", ", "-")}'
        for cls in encoder.classes_
    ]

    for key in _proba_names:
        predictions[key] = np.full_like(
            mask,
            OUTPUT_NODATA,
            dtype=OUTPUT_DTYPE,
        )

    if mask.any():
        predictions[target][mask] = model.predict(X).astype(OUTPUT_DTYPE)
        _proba_predictions = model.predict_proba(X) * 100
        _proba_predictions = _proba_predictions.round().astype(OUTPUT_DTYPE)
        for key, val in zip(_proba_names, _proba_predictions.T):
            predictions[key][mask] = val

    return predictions

def write_tile(i, window_dim, mask_negative=True):
    __, wtransform = make_window(i, window_dim)
    def wrapped(args):
        target, data = args
        if mask_negative:
            data[data<0] = OUTPUT_NODATA # sub-zero prediction artefacts
        outfile = _get_tile_fn(target, i)
        Path(outfile).parent.mkdir(parents=True, exist_ok=True)
        try:
            with rio.open(
                outfile, 'w',
                **{
                    **OUT_PROFILE,
                    **wtransform,
                }
            ) as dst:
                dst.write(data.astype(OUTPUT_DTYPE), 1)
        except rio.errors.RasterioIOError:
            os.makedirs(outfile.parent, exist_ok=True)
            wrapped(args)
    return wrapped

def write_to_mosaic(i, window_dim):
    window, __ = make_window(i, window_dim)
    def wrapped(args):
        target, data = args
        data[data<0] = OUTPUT_NODATA # sub-zero prediction artefacts
        outfile = outdir.joinpath(target+'.tif')

        if outfile.exists():
            mode = 'r+'
        else:
            mode = 'w'

        with rio.open(
            outfile, mode,
            **OUT_PROFILE,
        ) as dst:
            dst.write(data.astype(OUTPUT_DTYPE), 1, window=window)
    return wrapped

def do_predict():   

    target = target_lvl0
    pp = PreProcess(fn_models_pp)
    fn_layers = get_predictors_layers()
    layers = list(filter(lambda x: x.with_suffix('').name in pp.input_columns, fn_layers))  
    maskfile = fld_data_root.joinpath('clm_ece_mask.tif')

    n_tiles = raster_n_tiles(predict_window_dim)
    
    encoder = pickle.load(open(fld_models_root/f'models{ver}/{target}/{target}_encoder.pickle', 'rb'))
    model = pickle.load(open(fld_models_root/f'models{ver}/{target}/{target}.meta.pickle', 'rb'))

    try:
        with ThreadPool(len(layers)) as io_pool:
            with rio.open(maskfile) as mask_src:
                for i in range(n_tiles):
                    if _tile_exists(i, ):
                        continue
                    
                    window, __ = make_window(i, predict_window_dim)

                    ttprint(f'reading tile {i+1} of {n_tiles}')
                    mask = mask_src.read(1, window=window).astype(bool)

                    if mask.any():
                        X = pd.DataFrame(dict(io_pool.map(
                            read_layer(window, mask),
                            layers,
                        )))
                        
                        ttprint(f'tile {i+1}: preprocessing...')
                        X = pp.process(X)

                        ttprint(f'tile {i+1}: inferring...')                        
                        predictions = parse_window(X, mask, encoder, model)
                        

                        ttprint(f'tile {i+1}: writing...')
                        if write_individual_tiles:
                            __ = io_pool.map(
                                write_tile(i, window_dim),
                                predictions.items(),
                            )
                        else:
                            __ = io_pool.map(
                                write_to_mosaic(i, window_dim),
                                predictions.items(),
                            )
                        
                        ttprint(f'tile {i+1}: done.')

                        del X, predictions 

                    del window, mask, __
                    gc.collect()

        ttprint(f'Done all.')

    except KeyboardInterrupt as e:
        raise e

    except MemoryError as e:
        ttprint(f'ERROR: {str(e)}\nrestarting...')
        os.execl(
            sys.executable,
            sys.executable,
            *sys.argv,
        )

#%%

if __name__ == '__main__':
    import sys
    
    if len(sys.argv)>1:
        ver = sys.argv[1]
        ver = f'_{ver}'
    else:
        ver=''
    
    write_individual_tiles = True
    window_dim = 1024

    outdir = fld_data_predictions/f'predictions{ver}'

    do_predict()