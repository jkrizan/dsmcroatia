from pathlib import Path
import os, sys
from functools import reduce
from operator import add
import rasterio as rio
from multiprocessing.pool import ThreadPool
import multiprocessing as mp

# try:
#     _working_dir = Path(__file__).parent.parent.resolve().as_posix()
#     sys.path.append(_working_dir)
# except NameError:
#     _working_dir = Path(os.getcwd())/'dsmcroatia'
#     sys.path.append(_working_dir.as_posix())

from utils import (
    merge_tiles,
    ttprint,
    raster_get_profile,
)

from settings import(
    targets,
    fld_data_predictions,
)

def get_tile_sets(target):
    tiledir = (outdir/'tiles')
    dirs = [tiledir/f'{prefix}{target}{suffix}' 
                for prefix in ['','ln_'] 
                for suffix in ['','_q05','_q95','_qrange']
            ]
    ret=[]
    for dir in dirs:
        if dir.exists():
            ret.append((dir.name, [*dir.glob('*.tif')]))
    
    # probability for classification
    dirs = tiledir.glob(f'{target}_proba_*')
    for dir in dirs:
        ret.append((dir.name, [*dir.glob('*.tif')]))

    return ret


def merge_tileset(args):
    tset, tile_paths = args
    with rio.open(tile_paths[0]) as ref:
        profile = raster_get_profile(
            nodata=ref.nodata,
            dtype=ref.dtypes[0],
        )

    outfile = outdir/'merged'/f'{tset}.tif'

    merge_tiles(
        tile_paths,
        outfile,
        profile,
    )

    return tset

#%%
if __name__ == '__main__':
    import sys

    target = sys.argv[1]
    if len(sys.argv)>2:
        ver = sys.argv[2]
        ver = f'_{ver}'
    else:
        ver=''

    if target== 'all':
        selected_targets=targets
    elif ',' in target:
        selected_targets = list(map(lambda x: x.strip(), target.split(',')))
    else:
        selected_targets=[target]

    outdir = fld_data_predictions/f'predictions{ver}'

    tile_sets = reduce(add, map(
        get_tile_sets,
        selected_targets,
    ))

    n_jobs = mp.cpu_count()//2

    with ThreadPool(n_jobs) as io_pool:
        for i, tset in enumerate(io_pool.imap_unordered(
            merge_tileset,
            tile_sets,
        )):
            ttprint(f'merged {tset} / {i+1} of {len(tile_sets)} tilesets')
