from numpy.core.numeric import zeros_like
import rasterio as rio
import numpy as np
from pathlib import Path
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from subprocess import run

from settings import fld_data_predictions, class_targets
from utils import ttprint

#%%

def rat_wrb(wrb_raster):
    import gdal
    if isinstance(wrb_raster,Path):
        wrb_raster = wrb_raster.as_posix()

    classes = ['Anthrosols', 'Cambisols', 'Dystric Cambisols', 'Eutric Cambisols', 'Leptic Cambisols',
                'Chernozems', 'Fluvisols', 'Gleysols', 'Leptosols', 'Luvisols', 'Phaeozems', 'Podzols',
                'Regosols', 'Stagnosols', 'Umbrisols', 'Vertisols']
    # print('\n'.join(f'{c} - {i+1}' for i,c in enumerate(classes)))
    
    # https://gdal.org/python/osgeo.gdal.RasterAttributeTable-class.html
    # https://gdal.org/python/osgeo.gdalconst-module.html
    ds = gdal.Open(wrb_raster)
    rb = ds.GetRasterBand(1)

    # Create and populate the RAT
    rat = gdal.RasterAttributeTable()

    rat.CreateColumn('VALUE', gdal.GFT_Integer, gdal.GFU_Generic)
    rat.CreateColumn('TEXTURE_CLASS', gdal.GFT_String, gdal.GFU_Generic)

    for i, desc in enumerate(classes):
        rat.SetValueAsInt(i, 0, i)
        rat.SetValueAsString(i, 1, desc)

    # Associate with the band
    rb.SetDefaultRAT(rat)

    # Close the dataset and persist the RAT
    ds = None


OUT_DTYPE = np.uint8
OUT_NODATA = 255

def _read_layer(mask):
    def wrapped(fn):
        with rio.open(fn) as src:
            return src.read(1)[mask]
    return wrapped

def do_postprocess(ver):
    fld = fld_data_predictions/f'predictions{ver}/merged'
    proba_files = sorted(fld.glob('*_proba_*.tif'))
    ref = rio.open(proba_files[0])
    mask = ref.read_masks(1).astype(bool)
    output = np.full_like(mask, OUT_NODATA, OUT_DTYPE)
    output[mask] = 0
    profile = ref.profile
    profile.update(dtype=OUT_DTYPE, nodata=OUT_NODATA)

    n_classes = len(proba_files)
    n_jobs = mp.cpu_count()//2

    ttprint('calculating prediction entropy...')
    with ThreadPool(n_jobs) as io_pool:
        for i, data in enumerate(io_pool.imap_unordered(
            _read_layer(mask),
            proba_files,
        )):
            ttprint(f'parsing layer {i+1} of {n_classes}')
            data = data / 100
            data = np.maximum(data, 1e-15)
            output[mask] += (-100 * data * np.log2(data) / np.log2(n_classes)).round().astype(OUT_DTYPE)

    ttprint('writing to file...')
    outfile = fld/f'{class_targets[0]}_prediction_entropy.tif'
    with rio.open(outfile, 'w', **profile) as dst:
        dst.write(output, 1)    

    ttprint('done.')



if __name__=='__main__':
    import sys
    if len(sys.argv)>1:
        ver=f'_{sys.argv[1]}'
    else:
        ver=''
      
    
    do_postprocess(ver)

    fld = fld_data_predictions/f'predictions{ver}/merged'
    rat_wrb(fld/'wrb_rsg.tif')