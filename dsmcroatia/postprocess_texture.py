from numpy.core.numeric import zeros_like
import rasterio as rio
import numpy as np
from pathlib import Path
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from subprocess import run

from settings import fld_data_predictions

#%%
def rat(txt_raster):
    import gdal
    if isinstance(txt_raster,Path):
        txt_raster = txt_raster.as_posix()

    classes = ['Clay', 'Silty Clay', 'Silty Clay Loam', 'Sandy Clay', 'Sandy Clay Loam',
               'Clay Loam', 'Silt', 'Silt Loam', 'Loam', 'Sand', 'Loamy Sand', 'Sandy Loam']
    # print('\n'.join(f'{c} - {i+1}' for i,c in enumerate(classes)))
    
    # https://gdal.org/python/osgeo.gdal.RasterAttributeTable-class.html
    # https://gdal.org/python/osgeo.gdalconst-module.html
    ds = gdal.Open(txt_raster)
    rb = ds.GetRasterBand(1)

    # Create and populate the RAT
    rat = gdal.RasterAttributeTable()

    rat.CreateColumn('VALUE', gdal.GFT_Integer, gdal.GFU_Generic)
    rat.CreateColumn('TEXTURE_CLASS', gdal.GFT_String, gdal.GFU_Generic)

    for i, desc in enumerate(classes):
        rat.SetValueAsInt(i, 0, i+1)
        rat.SetValueAsString(i, 1, desc)

    # Associate with the band
    rb.SetDefaultRAT(rat)

    # Close the dataset and persist the RAT
    ds = None

def texture_class(sand, silt, clay):
    def sc(classnum, claymin, claymax, siltmin, siltmax, sandmin, sandmax):
        ind = (claymin<=clay) & (clay<=claymax) & (siltmin<=silt) & (silt<=siltmax) & (sandmin<=sand) & (sand<=sandmax)
        out[ind] = classnum

    sand = (sand*100).astype(np.uint8)
    silt = (silt*100).astype(np.uint8)
    clay = (clay*100).astype(np.uint8)

    out = zeros_like(sand)
    ## BUG !!!!
    #classes = ['Clay', 'Silty Clay', 'Silty Clay Loam', 'Sandy Clay', 'Sandy Clay Loam',
    #           'Clay Loam', 'Silt', 'Silt Loam', 'Loam', 'Sand', 'Loamy Sand', 'Sandy Loam']
    sc(7,  0,  15, 80, 100,  0,  20)
    sc(10,  0,  10,  0,  15, 85, 100)
    sc(11, 10,  15,  0,  30, 70,  85)
    sc(8,   0,  28, 50, 100,  0,  50)
    sc(12,  15,  20,  0,  50, 45,  85)
    sc(9,   8,  28, 28,  50, 22,  52)
    sc(6,  28,  40, 15,  52, 20,  45)
    sc(3,  28,  40, 40,  70,  0,  20)
    sc(5,  20,  35,  0,  28, 45,  80)
    sc(2,  40,  60, 40,  60,  0,  20)
    sc(4,  35,  55,  0,  20, 45,  65)
    sc(1,  55, 100,  0,  40, 20,  45)   

    return out 

    '''
    def classification (clay,silt,sand,x,y):
    soil_texture_class = 0
    if 55 <= clay[x][y] <= 100 and 0 <= silt[x][y] <= 40 and 20 <= sand[x][y] <= 45:
      soil_texture_class = 1 #clay
    elif 35 <= clay[x][y] <= 55 and 0 <= silt[x][y] <= 20 and 45 <= sand[x][y] <= 65:
      soil_texture_class = 2 #sandy clay
    elif 40 <= clay[x][y] <= 60 and 40 <= silt[x][y] <= 60 and 0 <= sand[x][y] <= 20:
      soil_texture_class = 3 #silty clay
    elif 20 <= clay[x][y] <= 35 and 0 <= silt[x][y] <= 28 and 45 <= sand[x][y] <= 80:
      soil_texture_class = 4 #sandy clay loam
    elif 28 <= clay[x][y] <= 40 and 40 <= silt[x][y] <= 70 and 0 <= sand[x][y] <= 20:
      soil_texture_class = 5 #silty clay loam
    elif 28 <= clay[x][y] <= 40 and 15 <= silt[x][y] <= 52 and 20 <= sand[x][y] <= 45:
      soil_texture_class = 6 #clay loam
    elif 8 <= clay[x][y] <= 28 and 28 <= silt[x][y] <= 50 and 22 <= sand[x][y] <= 52:
      soil_texture_class = 7 #loam
    elif 15 <= clay[x][y] <= 20 and 0 <= silt[x][y] <= 50 and 45 <= sand[x][y] <= 85:
      soil_texture_class = 8 #sandy loam
    elif 0 <= clay[x][y] <= 28 and 50 <= silt[x][y] <= 100 and 0 <= sand[x][y] <= 50:
      soil_texture_class = 9 #silt loam
    elif 10 <= clay[x][y] <= 15 and 0 <= silt[x][y] <= 30 and 70 <= sand[x][y] <= 85:
      soil_texture_class = 10 #loamy sand
    elif 0 <= clay[x][y] <= 10 and 0 <= silt[x][y] <= 15 and 85 <= sand[x][y] <= 100:
      soil_texture_class = 11 #sand
    elif 0 <= clay[x][y] <= 15 and 80 <= silt[x][y] <= 100 and 0 <= sand[x][y] <= 20:
      soil_texture_class = 12 #silt
    return soil_texture_class
    '''


def write_normalized(fn, data, mask, profile):

    with rio.open(fn, 'w', **profile) as dst:
        out = np.full_like(mask, profile['nodata'], dtype=profile['dtype'])
        out[mask] = data
        dst.write(out, 1)

def do_postprocess(ver:str):

    fld = fld_data_predictions/f'predictions{ver}/merged'

    texture_vars = ('sand', 'silt', 'clay')
    sufixes = ('q05','q95','qrange')
    
    input_fns = [fld/f'{tv}_tot_psa{ver}.tif' for tv in texture_vars]

    ref = rio.open(input_fns[0])
    mask = ref.read_masks(1).astype(bool)
    profile = ref.profile

    input_data = np.stack([rio.open(fn).read(1)[mask] for fn in input_fns])

    scale_factor = 1.0/input_data.sum(axis=0)

    norm_data = input_data * scale_factor
    for tv, data in zip(texture_vars, norm_data):
        write_normalized(fld/f'{tv}_tot_psa_norm{ver}.tif', data, mask, profile)

    for suf in sufixes:
        for tv in texture_vars:
            data = rio.open(fld/f'{tv}_tot_psa_{suf}{ver}.tif').read(1)[mask] * scale_factor
            write_normalized(fld/f'{tv}_tot_psa_{suf}{ver}_norm.tif', data, mask, profile)


    texture_data = texture_class(norm_data[0], norm_data[1], norm_data[2])

    profile['dtype'] = rio.uint8
    profile['nodata'] = 0
    write_normalized(fld/f'texture{ver}.tif', texture_data, mask, profile)

    rat(fld/f'texture{ver}.tif')    

if __name__=='__main__':
    import sys
    if len(sys.argv)>1:
        ver=f'_{sys.argv[1]}'
    else:
        ver=''
      
    do_postprocess(ver)