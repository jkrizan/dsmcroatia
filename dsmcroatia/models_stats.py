#%%


from settings import fn_models_pp, fld_data_sample, fld_models_root
from settings import non_logtrans, tile_dim, targets
from utils import PreProcess, log_pack, log_unpack, calc_r2_adj, MetaRegressor, tile_id, concordance_correlation_coefficient

from pathlib import Path
import pandas, pickle
import sklearn.model_selection as modsel
from sklearn import metrics
from sklearn.metrics import make_scorer, get_scorer
import multiprocessing as mp

import json


fn_sample_points = fld_data_sample/'sample_points.csv'

# override for v15
'''
fn_models_pp = Path('/home/josip/repos/wbsoilhr/model/preproc_v15.pickle')
fn_sample_points = Path('/data/wbsoilhr/sample_overlay.csv') 
ver='_v15'
target='oc'
'''
#%%
def get_model_stats(model, x, y, cv, g, logtarget, n_jobs=mp.cpu_count()-1):
    # x=X; y=Y; cv=kfold; g=G; n_jobs=mp.cpu_count()-1
    score_ccc = make_scorer(concordance_correlation_coefficient)

    n, p = x.shape
    n_splits = cv.n_splits
    cv_results = model.cross_validate(
        x, y, cv=cv, groups=g, 
        scoring={'ccc':score_ccc, 'r2':get_scorer('r2'), 'mse':get_scorer('neg_mean_squared_error')},
        n_jobs=n_jobs,
        return_train_score=True,
    )
    prd = model.predict(x)
    if logtarget:
        prd = log_unpack(prd)

    r2_all = metrics.r2_score(y, prd)
    ret={   'r2_all': r2_all,
            'r2_val': cv_results['test_r2'].mean(),
            'r2_train': cv_results['train_r2'].mean(),
            'r2_adj_all': calc_r2_adj(r2_all, p, n),
            'r2_adj_val': calc_r2_adj(cv_results['test_r2'].mean(), p, (n_splits-1)*n//n_splits),
            'r2_adj_train': calc_r2_adj(cv_results['train_r2'].mean(), p, (n_splits-1)*n//n_splits),
            'ccc_all': concordance_correlation_coefficient(y, prd),
            'ccc_val':  cv_results['test_ccc'].mean(),
            'ccc_train': cv_results['train_ccc'].mean(),
            'mse_all': metrics.mean_squared_error(y, prd),
            'mse_val': -cv_results['test_mse'].mean(),
            'mse_train': -cv_results['train_mse'].mean(),
            'n': n
            }

    return ret
#%%

def update_model_stats(sel_targets, ver):
    pp = PreProcess(fn_models_pp)
    df = pp.process_overlay(fn_sample_points)
    kfold = modsel.GroupKFold(n_splits=5)
    groups = tile_id(df, tile_dim)

    models_dir = fld_models_root/f'models{ver}'

    fn_models_stats = models_dir/f'models{ver}_stats'
    fn_models_stats_csv = fn_models_stats.with_suffix('.csv')
    if fn_models_stats_csv.exists(): 
        dfs = pandas.read_csv(fn_models_stats_csv, sep='\t').set_index('target')
        all_stats = dfs.to_dict(orient='index')
    else:
        dfs = pandas.DataFrame(columns=['target','n','r2_all','r2_val','r2_train','r2_adj_all','r2_adj_val','r2_adj_train','ccc_all','ccc_val','ccc_train','mse_all','mse_val','mse_train'])
        all_stats={}

    for target in sel_targets:
        Y = df[[target]].copy()
        idx = ~Y.isna().values.ravel()
        Y = Y[idx].values.ravel()
        X = df.loc[idx, df.columns[30:]].copy()
        G = groups[idx].copy()

        fn_model = models_dir/f'{target}/{target}.meta.pickle'
        if not fn_model.exists():
            continue

        print(target)
        model = pickle.load(open(fn_model, 'rb'))

        if target in non_logtrans:
            stats = get_model_stats(model, X, Y, kfold, G, logtarget=False)
            #stats['target'] = target
            #dfs=dfs.append(stats)
            all_stats[target] = stats
        else:
            stats = get_model_stats(model, X, Y, kfold, G, logtarget=True)
            #stats['target'] = target
            #dfs=dfs.append(stats, ignore_index=True)
            all_stats[target] = stats

            Y_ = log_pack(Y)
            stats = get_model_stats(model, X, Y_, kfold, G, logtarget=False)
            #stats['target'] = f'ln({target})'
            #dfs=dfs.append(stats, ignore_index=True)
            all_stats[f'ln({target})'] = stats

    
    dfs = pandas.DataFrame.from_dict(all_stats,orient='index')
    dfs.index.set_names('target', inplace=True)
    dfs.to_csv(fn_models_stats.with_suffix('.csv'), sep='\t')
    dfs.to_json(fn_models_stats.with_suffix('.json'), orient='index')


# def do_v15():
#     global fn_models_pp, fn_sample_points
#     global MaskedPCA, PreProcess, SubPCA

#     from utils.preprocess import MaskedPCA, SubPCA

#     fn_models_pp = Path('/home/josip/repos/wbsoilhr/model/preproc_v15.pickle')
#     fn_sample_points = Path('/data/wbsoilhr/sample_overlay.csv') 
#     ver='_v15'
 
#     update_model_stats(targets, ver)
    

if __name__=='__main__':
    import sys

    target = sys.argv[1]
    if len(sys.argv)>2:
        ver = sys.argv[2]
        ver = f'_{ver}'
    else:
        ver=''
                        

    print(f'Calculating stats for models for target: {target}, version: {ver}')
  
    if target== 'all':
        selected_targets=targets
    elif ',' in target:
        selected_targets = list(map(lambda x: x.strip(), target.split(',')))
    else:
        selected_targets=[target]
        
    update_model_stats(selected_targets, ver)
    