from pathlib import Path

# folder with this file
fld_src = Path(__file__).parent

# data folders
## root data folder, not used in other python scripts
fld_data_root = Path(r'/data/wbsoilhr')
## folder with all inputs
fld_data_inputs = fld_src.parent/'inputs'
## folder with compiled iput points database
fld_db = fld_src.parent/'hrtopsoildb'
## folder with all predictors
fld_data_predictors = fld_data_root/'predictors'
## folder with all predictions
fld_data_predictions = fld_data_root/'predictions'
## folder with samples
fld_data_sample = fld_data_root/'samples'
## file with samples for preprocessing
fn_sample_pp = fld_data_sample/'sample_preprocessing'
## folder with models
fld_models_root = fld_data_root/'models'
## file with preprocessing model
fn_models_pp = fld_models_root/'preprocessing/model_pp.pickle'

# some parameters
predict_window_dim = 1024
tile_dim = 20000    # meters, for spatial crossvalidation
models_search_niter = 500
models_search_nthreads = 10

# saga
#saga_soil_texture_cmd = 'saga_cmd grid_analysis 14'
#saga
# 
targets = [
    'oc',
    'n_tot_ncs',
    'ph_h2o',
    'ph_kcl',
    'clay_tot_psa',
    'silt_tot_psa',
    'sand_tot_psa',
    'db_od',
    'dbr',
    'caco3',
    'wpg2',
    'p_mehlich3',
    'k_mehlich3',
    'ca_mehlich3',
    'mg_mehlich3',
    'ec_satp',
    'cec_sum',
]

class_targets = ['wrb_rsg']

non_logtrans = [
      'sand_tot_psa',
      'silt_tot_psa',
      'clay_tot_psa',
      'ph_h2o',
      'ph_kcl',
      'db_od',
]

delete_zeros = [
    'oc',
]

def get_predictors_layers():
    pdir = fld_data_predictors
    fn_layers = sorted([
        fld_data_root/'soilmask.tif',
        *pdir.glob('clm/*.tif'),    
        *pdir.glob('clm_ece/*.tif'),
        *pdir.glob('dem_derivatives/*.tif'),
        *pdir.glob('gsw/*.tif'),
        *pdir.glob('s1_mosaics/*.tif'),
        *filter(lambda f: not f.name.endswith('_CNT.tif'), pdir.glob('s2_mosaics_v3/*.tif')),
        *pdir.glob('solar_atlas/*.tif'),
        pdir.joinpath('lit_comp.tif'),
        *pdir.glob('lit_comp/lit_comp_*.tif')
    ])

    return fn_layers

def get_points():
    import geopandas as gp
    
    pointfile = fld_db/'hr_topsoil_db.gpkg'
    print (f'Points imported from {pointfile}')

    points = gp.read_file(pointfile)
    points = points.to_crs('EPSG:3035')

    return points