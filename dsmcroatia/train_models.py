#%%
from pathlib import Path
import pandas as pd
import numpy as np
import sklearn.model_selection as modsel

from utils import PreProcess, tile_id, MetaRegressor, calc_r2_adj, ttprint, make_meta_stats, log_pack
from settings import fld_models_root, fn_models_pp, fld_data_sample
from settings import models_search_niter, models_search_nthreads, tile_dim
from settings import targets, non_logtrans, delete_zeros

from sklearn import (
    base
)

import pickle
from scipy import stats
import xgboost as xgb
import matplotlib.pyplot as plt

import os
import traceback

import warnings
warnings.filterwarnings('ignore')

models_init = {
    'GBT': (
        xgb.XGBRegressor(
            objective='reg:squarederror',
            tree_method='gpu_hist',
            booster='gbtree',
            # warm_start=True,
        ),
        {
            'eta': [0.3, 0.4, 0.5, 0.6], #v9            
            'gamma': stats.uniform(0, 20),
            'alpha': stats.uniform(0., 1.),
            'min_child_weight': stats.uniform(0.8, 9.2), #[default=1][range:(0,Inf)]
            'max_depth': stats.randint(3, 11), # default 3 v7
            'n_estimators': stats.randint(10, 50), # default 100 #v9
        },
    ),
    'GBLinear': (
        xgb.XGBRegressor(
            objective='reg:squarederror',
            # tree_method='gpu_hist',
            booster='gblinear',
            updater='coord_descent',
            feature_selector='thrifty',
        ),
        {
            'eta': stats.uniform(.01, .99),
            'alpha': stats.uniform(0., 1.),
            'n_estimators': stats.randint(10, 50), # default 100 #v9
            'top_k': stats.randint(10, 500),
        },
    ),
    'RF': (
        xgb.XGBRFRegressor(
            tree_method='gpu_hist',
            n_jobs=8,
        ),
        {
            'eta': stats.uniform(.01, .99),
            'colsample_bynode': stats.uniform(0.1, 0.9),
            'subsample': stats.uniform(.1, .9),
             'max_depth': stats.randint(2, 11), # default 3 #v7
            'n_estimators': stats.randint(10, 50), # default 100 v9
        },
    )
}

#%%
def train_target_ensemble(df:pd.DataFrame, groups: np.ndarray, target: str, model_type:str, ver:str, model_logarithms=True, search_niter=200, search_nthreads=10):

    models_dir = fld_models_root/f'models{ver}'
    print(models_dir)

    out_dir = models_dir.joinpath(target)
    if not out_dir.is_dir():
        os.makedirs(out_dir)

    Y = df[[target]].copy()
    idx = ~Y.isna().values.ravel()
    if target in delete_zeros:
        idx = idx & (Y.values.ravel() > 1e-15)

    X = df.loc[idx,df.columns[30:]].astype(np.float32)
    Y = Y[idx]
    Y_ = Y.values.astype(np.float32).ravel()
    if model_logarithms and target not in non_logtrans:
        Y_ = np.log(Y_ + 1)
    if model_type in ('NN',):
        X = X.values

    groups = groups[idx]
    kfold = modsel.GroupKFold(n_splits=5)

    model, params = models_init[model_type]

    search = modsel.RandomizedSearchCV(
        model,
        param_distributions=params,
        scoring='r2', #'neg_mean_squared_error',
        n_iter=search_niter,
        cv=kfold,
        verbose=1,
        n_jobs = search_nthreads,
        pre_dispatch=search_nthreads,
        return_train_score = True
    )

    search.fit(X, Y_, groups)

    dfcv = pd.DataFrame(search.cv_results_)
    dfcv.to_excel(out_dir/f'{model_type}_cvres.xlsx')
    dfcv.to_csv(out_dir/f'{model_type}_cvres.csv', sep='\t')

    with open(out_dir/f'{model_type}.model.pickle', 'wb') as f:
        pickle.dump(search, f)


def train_target_metaregressor(
    dataset: pd.DataFrame,
    groups: np.ndarray,
    target: str,
    ver,
    n_top_models: int=5,
    n_threads: int=1,
    print_stats: bool=False,
):
#%%

    models_dir = fld_models_root/f'models{ver}'    

    out_dir = models_dir.joinpath(target)
    out_file_prf = out_dir.joinpath(target+'.meta').as_posix()

    Y = dataset[[target]].copy()
    idx = ~Y.isna().values.ravel()
    Y_ = Y[idx].values.ravel()
    X = dataset[dataset.columns[30:]].copy()[idx]

    if target not in non_logtrans:
        logtarget = True
        Yg = Y_.copy()
        Y_ = log_pack(Y_)        
    else:
        logtarget = False
        Yg = Y_

    estimators = []
    for estimator_search in sorted(out_dir.glob('*.model.pickle')):
        with open(estimator_search, 'rb') as f:
            search = pickle.load(f)
        search_results = pd.DataFrame(search.cv_results_)
        test_score = search_results['mean_test_score']
        train_score = search_results['mean_train_score']

        rank_index = search_results.rank_test_score.sort_values().drop_duplicates()[:n_top_models].index        
        _est = search.estimator
        for model_i in rank_index:
            est = base.clone(_est).set_params(**search_results['params'][model_i])
            est.fit(X, Y_)
            estimators.append(est)
                        

    kfold = modsel.GroupKFold(n_splits=5)

    meta_learner = MetaRegressor(
        estimators,
        max_depth=5,   #5
        n_estimators=50,   #50
        learning_rate=.1,
    )
    meta_learner.fit(X, Y_, fit_quantile=True)

    with open(out_file_prf+'.pickle', 'wb') as f:
        pickle.dump(meta_learner, f)

    out_stats = []
    if print_stats:
        mean_validation_mse = -meta_learner.cross_validate(
            X, Y_,
            cv=kfold,
            groups=groups[idx],
            scoring='neg_mean_squared_error',
            n_jobs=n_threads,
        )['test_score'].mean().round(4)

        mean_validation_r2 = meta_learner.cross_validate(
            X, Y_,
            cv=kfold,
            groups=groups[idx],
            scoring='r2',
            n_jobs=n_threads,
        )['test_score'].mean().round(3)

        mean_validation_r2_adjusted = calc_r2_adj(mean_validation_r2, X.shape[1], X.shape[0]).round(3)
        out_stats = ['//', mean_validation_mse, mean_validation_r2, mean_validation_r2_adjusted]

    # Statistics and plots
    make_meta_stats(target, meta_learner, X, Yg, groups[idx], logtarget, out_file_prf, n_threads)

    ttprint(target, 'done', *out_stats)


# %%

def train_ensemble(df, target, ver, search_niter, search_nthreads):
    groups = tile_id(df, tile_dim)

    for model_type in ['GBLinear', 'GBT', 'RF']:
        print()
        print(' -- ' , target, model_type)
        try:
            #pass
            train_target_ensemble(df, groups, target, model_type, ver, model_logarithms=True, search_niter=search_niter, search_nthreads=search_nthreads)            
        except:
            print(f'train_target_ensemble({target},{model_type}) FAILED !!!')
            traceback.print_exc(file=sys.stdout)

    train_target_metaregressor(df, groups, target, ver, n_top_models=5, n_threads = search_nthreads, print_stats=True)            

if __name__=='__main__':
    import sys

    target = sys.argv[1]
    if len(sys.argv)>2:
        ver = sys.argv[2]
        ver = f'_{ver}'
    else:
        ver=''
            

    pp = PreProcess(fn_models_pp)
    df = pp.process_overlay(fld_data_sample/'sample_points.csv')

    print(f'Training models for target: {target}, version: {ver}')
  
    if target=='all':
        for target in targets:
            print(target)
            train_ensemble(df, target, ver, search_niter=models_search_niter, search_nthreads=models_search_nthreads)
    else:
        train_ensemble(df, target, ver, search_niter=models_search_niter, search_nthreads=models_search_nthreads)

# %%
