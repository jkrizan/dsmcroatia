
import numpy as np
import pandas
import sklearn.model_selection as modsel
from sklearn import metrics, preprocessing, ensemble, base
import pickle
import multiprocessing as mp
import xgboost as xgb
from scipy import stats
import os

from utils import PreProcess, tile_id, MetaRegressor, calc_r2_adj, ttprint, make_class_plots
from settings import fld_models_root, fn_models_pp, fld_data_sample
from settings import models_search_niter, models_search_nthreads, tile_dim
from settings import targets, non_logtrans, delete_zeros

target_lvl0 = 'wrb_rsg'
target_lvl1 = 'wrb_qual1'
target_lvl2 = 'wrb_qual2'

models_init = {
    'GBT': (
        xgb.XGBClassifier(
            objective='multi:softmax',
            tree_method='gpu_hist',
            # tree_method='hist',
            booster='gbtree',
        ),
        {
            'eta': stats.loguniform(.001, .9),
            'gamma': stats.uniform(0, 12),
            'alpha': stats.uniform(0., 1.),
            'max_depth': stats.randint(2, 10), #
            'n_estimators': stats.randint(10, 50), # default 100 #v9
        },
    ),
    'GBLinear': (
        xgb.XGBClassifier(
            objective='multi:softmax',
            # tree_method='gpu_hist',
            booster='gblinear',
            updater='coord_descent',
            feature_selector='thrifty',
        ),
        {
            'eta': stats.loguniform(.001, .9),
            'alpha': stats.uniform(0., 1.),
            'n_estimators': stats.randint(10, 50), # default 100 #v9
            'top_k': stats.randint(10, 500),
        },
    ),
    'RF': (
        xgb.XGBRFClassifier(
        #ensemble.RandomForestRegressor(
            objective='multi:softmax',
            tree_method='gpu_hist',
            # tree_method='hist',
            n_jobs=4,
        ),
        {
            'eta': stats.loguniform(.001, .9),
             'max_depth': stats.randint(2, 10), # default 3 #v7
            'n_estimators': stats.randint(10, 50), # default 100 v9
        },
    ),
}
#%%
def prepare_data(df):
    # make target as wrb_rsg for all classes but 'Cambisols',  Cambisols are expanded with wrb_qual1
    
    Y = df[target_lvl0].copy()
    Y_lvl1 = df[target_lvl1].copy()

    data_idx = ~Y.isna()
    lvl1_idx = ~Y_lvl1.isna()
    cambi_idx = Y == 'Cambisols'

    cls_expand_idx = lvl1_idx & cambi_idx

    Y[cls_expand_idx] = Y[cls_expand_idx] + ', ' + Y_lvl1[cls_expand_idx]

    labels, counts = np.unique(Y[data_idx], return_counts=True)
    for label, count in zip(labels, counts):
        if count < 6:
            Y[Y==label] = np.nan
    data_idx = ~Y.isna()

    encoder = preprocessing.LabelEncoder()
    encoder.fit(Y[data_idx])

    return encoder.transform(Y[data_idx]), data_idx, encoder

def train_classifier(
    model_type, X, Y, kfold,
    search_niter,
    search_nthreads,
):

    model, params = models_init[model_type]

    #scorer = metrics.make_scorer(metrics.log_loss, needs_proba=True, labels=Y)
    search = modsel.RandomizedSearchCV(
        model,
        param_distributions=params,        
        scoring='f1_weighted',        
        n_iter=search_niter,
        cv=kfold,
        verbose=1,
        n_jobs=search_nthreads,
        return_train_score=True,
    )
    search.fit(X.values, Y)
    return search

def validate_classifier(
    model, X, Y, cv,
    encoder, out_file_prf,
    search_nthreads,
    search_nfeatures=25,
):
    n_classes = np.unique(Y).size
    has_importances = True
    try:
        vip = model.feature_importances_
        ind = np.argsort(vip)[-search_nfeatures:][::-1]
    except AttributeError:
        has_importances = False

    f1_val = modsel.cross_val_score(
        model, X.values, Y,
        scoring='f1_weighted',
        cv=cv,
        verbose=1,
        n_jobs=search_nthreads,
    ).mean()

    precision_val = modsel.cross_val_score(
        model, X.values, Y,
        scoring='precision_weighted',
        cv=cv,
        verbose=1,
        n_jobs=search_nthreads,
    ).mean()

    recall_val = modsel.cross_val_score(
        model, X.values, Y,
        scoring='recall_weighted',
        cv=cv,
        verbose=1,
        n_jobs=search_nthreads,
    ).mean()

    pred_all = model.predict(X.values)
    cm = metrics.confusion_matrix(Y, pred_all).astype(int)

    cmhead = np.r_[[''], encoder.classes_].reshape(1, -1)
    cm4text = np.c_[encoder.classes_, cm]
    cm4text = np.r_[cmhead, cm4text]

    precision_all = metrics.precision_score(Y, pred_all, average='weighted')
    recall_all = metrics.recall_score(Y, pred_all, average='weighted')
    f1_all = metrics.f1_score(Y, pred_all, average='weighted')

    n_samples = Y.size

    nl = '\n'
    report_col_width = 25
    report_border_width = 100

    summary = f'''
{target_lvl0}
XGB classifier results; N samples: {n_samples}

Mean score over validation fold
{'-'*report_border_width}
weighted precision:  {precision_val}
weighted recall:     {recall_val}
weighted f1:         {f1_val}

Score over entire target set
{'-'*report_border_width}
weighted precision:  {precision_all}
weighted recall:     {recall_all}
weighted f1:         {f1_all}

confusion matrix: true (y) vs predicted (x)
{'-'*report_border_width}
{nl.join([
    ''.join([
        f'{v}'.ljust(report_col_width)
        for v in row
    ])
    for row in cm4text
])}
'''
    if has_importances:
        summary += f'''
{search_nfeatures} most important features
{'-'*report_border_width}
{' '*report_col_width + 'importance'.ljust(report_col_width) + 'feature'.ljust(report_col_width)}
{'-'*report_border_width}
{nl.join([
('%d'%i).ljust(report_col_width) + ('%f'%imp).ljust(report_col_width) + feat_name
for i, imp, feat_name in zip(ind, vip[ind], X.columns[ind])
])}
{'-'*report_border_width}
    '''

    # print(summary)

    with open(out_file_prf+'_summary.txt', 'w') as f:
        f.write(summary)

    make_class_plots(Y, cm, target_lvl0, encoder, out_file_prf)

def get_estimators(searches, n_top_models=5):

    estimators = []
    weights = []
    for model_type, search in searches:
        search_results = pandas.DataFrame(search.cv_results_)
        test_score = search_results['mean_test_score']
        train_score = search_results['mean_train_score']

        search_score = test_score * np.maximum(1, test_score/train_score)

        _est = search.best_estimator_
        for model_rank, model_i in enumerate(np.argsort(
            search_score,
        )[-n_top_models:][::-1]):
            estimators.append((
                f'{model_type}_{model_rank+1}',
                base.clone(_est),
            ))
            estimators[-1][1].set_params(**search_results['params'][model_i])            
            weights.append(search_score[model_i])

    return estimators, weights


def train_ensemble(df, ver, search_niter, search_nthreads):

    models_dir = fld_models_root/f'models{ver}'    
    out_dir = models_dir.joinpath(target_lvl0)
    if not out_dir.is_dir():
        os.makedirs(out_dir)


    #groups = tile_id(df, tile_dim)

    Y, data_idx, encoder = prepare_data(df)
    X = df.loc[data_idx, df.columns[30:]]

    with open(out_dir/f'{target_lvl0}_encoder.pickle', 'wb') as f:
        pickle.dump(encoder, f)

    #n_threads = mp.cpu_count()/2
    kfold = modsel.StratifiedKFold(n_splits=5)

    searches = []
    for model_type in ('GBT', 'RF', 'GBLinear'):            
        ttprint('training', model_type)

        search = train_classifier(
            model_type, X, Y, kfold,
            search_niter = search_niter,
            search_nthreads = search_nthreads,
        )

        out_file_prf = out_dir.joinpath(f'{target_lvl0}_{model_type}').as_posix()
        with open(out_file_prf+'.search.pickle', 'wb') as f:
            pickle.dump(search, f)

        ttprint('validating', model_type)

        validate_classifier(
            search.best_estimator_,
            X, Y, kfold,
            encoder,
            out_file_prf,
            search_nthreads,
        )

        searches.append((model_type, search))

    ttprint('weak learner training done')

    out_file_prf = out_dir.joinpath(f'{target_lvl0}.meta').as_posix()

    estimators, weights = get_estimators(searches)

    meta_learner = ensemble.VotingClassifier(
        estimators,
        voting='soft',
        n_jobs=search_nthreads,
        weights=weights,
    )

    ttprint('training meta-learner')
    meta_learner.fit(X, Y)

    ttprint('validating meta-learner')
    validate_classifier(
        meta_learner,
        X, Y, kfold,
        encoder,
        out_file_prf,
        search_nthreads,    
    )

    with open(out_file_prf+'.pickle', 'wb') as f:
        pickle.dump(meta_learner, f)

    ttprint('Done.')


if __name__=='__main__':
    import sys

    if len(sys.argv)>1:
        ver = sys.argv[1]
        ver = f'_{ver}'
    else:
        ver=''

    pp = PreProcess(fn_models_pp)
    df = pp.process_overlay(fld_data_sample/'sample_points.csv')

    train_ensemble(df, ver, models_search_niter, models_search_nthreads)