#%%
import geopandas
import numpy as np
from numpy.lib.shape_base import get_array_prepare
import pandas
from pathlib import Path
from utils import SamplePointsParallel
from utils import ttprint
import rasterio
from settings import fld_data_root, fld_data_sample, get_predictors_layers, fn_sample_pp
import multiprocessing as mp



#%%

def sample_for_pca(fn_layers, fn_outfile: Path, n_samples=1000000):

    with rasterio.open(fld_data_root/'soilmask.tif') as src:
        transform = src.transform
        soilmask = src.read(1)

    #mask_height, mask_width = soilmask.shape

    mask_sum = soilmask.sum()
    mask_random = np.zeros(mask_sum, dtype='uint8')
    mask_random[:n_samples]=1
    np.random.shuffle(mask_random)
    
    soilmask[soilmask==1] = mask_random

    rows, cols = soilmask.nonzero()

    ptsx, ptsy = rasterio.transform.xy(transform, rows, cols)
    ptsx = np.array(ptsx)
    ptsy = np.array(ptsy)
#%%
    max_workers = max(mp.cpu_count()-1, 1)    
    df = pandas.DataFrame({'x':ptsx, 'y':ptsy})
    
    ttprint('Preparing...')
    spp = SamplePointsParallel(ptsx, ptsy, fn_layers, fn_outfile, max_workers)
    ttprint(f'Sampling in {max_workers} threads...')
    res = spp.sample_v4()

    for col in sorted(res.keys()):
        df[col] = res[col]

    df = df[df['soilmask']==1]

    ttprint('Saving parquet file ...')
    #df.to_csv(fn_outfile.as_posix()+'.csv', sep='\t', index=False)
    df.to_parquet(fn_outfile.as_posix()+'.parquet')

    ttprint('Saving shp...')    
    dfg = geopandas.GeoDataFrame()
    dfg.geometry = geopandas.points_from_xy(ptsx,ptsy)
    dfg.to_file(fn_outfile.as_posix()+'.gpkg', driver='GPKG')

    ttprint('...done.')
# %%

if __name__=='__main__':
    sample_for_pca(get_predictors_layers(), fn_sample_pp, 1000000)