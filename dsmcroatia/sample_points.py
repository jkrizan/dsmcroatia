#%%
from pathlib import Path
from settings import fld_data_sample, get_predictors_layers, get_points

import multiprocessing as mp

from utils import SamplePointsParallel
from utils import ttprint

fn_outfile = fld_data_sample/'sample_points'

#%%

if __name__ == '__main__':
    fn_layers = get_predictors_layers()
    df = get_points()

    df['x'] = df.geometry.x
    df['y'] = df.geometry.y

    max_workers = mp.cpu_count()

    ptsx = df.geometry.x.values
    ptsy = df.geometry.y.values

    ttprint('Preparing...')
    spp = SamplePointsParallel(ptsx, ptsy, fn_layers, fn_outfile, max_workers)
    ttprint(f'Sampling in {max_workers} threads...')
    res = spp.sample_v4()

    for col in sorted(res.keys()):
        df[col] = res[col]

    df = df[df['soilmask']==1]

    ttprint('Saving csv...')
    df.to_csv(fn_outfile.as_posix()+'.csv', sep='\t', index=False)
    ttprint('Saving shp...')
    df[['geometry']].to_file(fn_outfile.as_posix()+'.gpkg', driver='GPKG')
    ttprint('...done.')
