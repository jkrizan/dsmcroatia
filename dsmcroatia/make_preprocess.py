#%%
import pandas
import pickle
from utils import ttprint

from settings import fn_sample_pp, fn_models_pp
from utils.preprocess import MaskedPCA

#%%
def fit_and_save_preprocessing():
    ttprint('Reading input samples ...')
    X = pandas.read_parquet(fn_sample_pp.with_suffix('.parquet'))

    cols_to_drop = X.columns.str.contains('^ssm_') \
                    | X.columns.str.contains('^dtm_flow_dem') \
                    | X.columns.str.contains('soilmask')  \
                    | (X.columns=='x') \
                    | (X.columns=='y') 
    X = X.loc[:,~cols_to_drop]  # 533 cols   # v13 -> 495 columns

    pcas_description = {
            'clm_ece': '^clm_ece',
            'clm_lst': '^clm_lst',
            'clm_bioclim': '^clm_bioclim',
            'clm_temp': '^clm_monthly[.]temp',
            'clm_cloud': '^clm_cloud',
            'clm_prec': '^clm_precipitation',
            'clm_snow': '^clm_snow',
            'clm_water': '^clm_water',
            'dtm': '^dtm_',
            's1': '^s1_',
            's2': '^s2l2a_\d+_s[234]',  # without winter
            'gsw': ('^gsw'),
            'solar_atlas': '^sola_',
            'lit': '^lit_comp'
            }

    mpca = MaskedPCA(X.columns.to_list(), pcas_description=pcas_description, scaling=True)

    ttprint('Fitting preproc ...')
    
    mpca.fit(X[mpca.input_columns])

    ttprint(('Pickling ...'))
    #with open(f'preproc_{version}.pickle', 'wb') as f:
    with open(fn_models_pp, 'wb') as f:
        pickle.dump(mpca, f)

    ttprint('Done.')


if __name__ == '__main__':
    fit_and_save_preprocessing()