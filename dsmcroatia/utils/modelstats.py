import multiprocessing
import pandas as pd
import numpy as np
import multiprocessing as mp
from . import ttprint, calc_r2_adj, log_unpack, log_pack

from sklearn import metrics
import sklearn.model_selection as modsel
from sklearn.metrics import make_scorer, get_scorer
from . import concordance_correlation_coefficient
score_ccc = make_scorer(concordance_correlation_coefficient)

import matplotlib.patches as mpatches

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
plt.style.use('ggplot')

cmap = plt.cm.Reds
alpha_cmap = cmap(np.arange(cmap.N))
alpha_cmap[0,-1] = 0.
alpha_cmap[1:,-1] = np.linspace(.75, 1, cmap.N-1)
alpha_cmap = ListedColormap(alpha_cmap)

def make_meta_stats(target, meta_learner, X, Y_, groups, logtarget, output_prefix, n_threads):

    target_name = target.replace("_", " ")
    kfold = modsel.GroupKFold(n_splits=5)

    cv_results = meta_learner.cross_validate(
        X, Y_,
        cv=kfold,
        groups=groups,
        scoring={'ccc':score_ccc, 'r2':get_scorer('r2'), 'mse':get_scorer('neg_mean_squared_error')} , #('r2', 'neg_mean_squared_error',score_ccc),
        n_jobs=n_threads,
        return_train_score=True,
    )

    mean_validation_mse = -cv_results['test_mse'].mean().round(4)
    mean_validation_r2 = cv_results['test_r2'].mean().round(3)
    mean_validation_ccc = cv_results['test_ccc'].mean().round(3)
    mean_train_mse = -cv_results['train_mse'].mean().round(4)
    mean_train_r2 = cv_results['train_r2'].mean().round(3)
    mean_train_ccc = cv_results['train_ccc'].mean().round(3)

    mean_validation_r2_adjusted = calc_r2_adj(mean_validation_r2, X.shape[1], X.shape[0]//5).round(3)
    mean_train_r2_adjusted = calc_r2_adj(mean_train_r2, X.shape[1], 4*X.shape[0]//5).round(3)

    #%%
    predicted, q05, q95 = meta_learner.predict(X, return_interval=True)
    if logtarget:
        predicted, q05, q95 = map(log_unpack, (predicted, q05, q95))
    
               
    total_mse = metrics.mean_squared_error(Y_, predicted).round(4)
    total_r2 = metrics.r2_score(Y_, predicted).round(3)
    total_r2_adjusted = calc_r2_adj(total_r2, X.shape[1], X.shape[0]).round(3)
    total_ccc = concordance_correlation_coefficient(Y_, predicted).round(3)

    qbin = int(Y_.size * .1)

    dq05 = q05 - predicted
    dq95 = q95 - predicted
    dq05 = pd.Series(dq05).rolling(qbin).mean()
    dq95 = pd.Series(dq95).rolling(qbin).mean()

    plot_title_info = f' $(n={len(Y_)})$ \n$ '\
        f'r^2={total_r2}, '\
        f'r^2_{{val}}={mean_validation_r2}, '\
        f'r^2_{{train}}={mean_train_r2}, '\
        f'r^2_{{adj}}={total_r2_adjusted}, '\
        f'r^2_{{adj val}}={mean_validation_r2_adjusted}, '\
        f'r^2_{{adj train}}={mean_train_r2_adjusted}, $\n$'\
        f'ccc={total_ccc}, '\
        f'ccc_{{val}}={mean_validation_ccc}, '\
        f'ccc_{{train}}={mean_train_ccc}, $\n$'\
        f'mse={total_mse}, '\
        f'mse_{{val}}={mean_validation_mse}, '\
        f'mse_{{train}}={mean_train_mse}$'

    make_plot(output_prefix+'_scatter.png', Y_, predicted, dq05, dq95, qbin, False, target_name+plot_title_info)
    if logtarget:
        make_plot(output_prefix+'_log_scatter.png', Y_, predicted, dq05, dq95, qbin, True, target_name+plot_title_info)


def make_plot(out_file, Y_, predicted, dq05, dq95, qbin, logtarget, title):
    from matplotlib import rc
    rc('text', usetex=True)

    fig, ax = plt.subplots(figsize=(10, 7))
    if logtarget:
        ax.set_xscale('log')
        ax.set_yscale('log')
        scale = 'log'
        ind = (Y_<=0) | (predicted<=0)
        Y_[ind] = 0.01
        predicted[ind] = 0.01
    else:
        scale='linear'

    hb = ax.hexbin(
        Y_.ravel(),
        predicted.ravel(),
        gridsize=50,
        cmap=alpha_cmap,
        edgecolors='none',
        xscale=scale,
        yscale=scale
    )
    ax.axis([
        Y_.min(),
        Y_.max(),
        predicted.min(),
        predicted.max(),
    ])
    _sort = Y_.argsort()
    ax.plot(
        Y_[_sort],
        Y_[_sort],
        linestyle=(0, (3, 5, 1, 5)),
        color='black',
        label='predicted = measured'
    )
    ax.plot(
        Y_[_sort],
        (Y_+dq05)[_sort],
        color='#bbb',
        linewidth=.5,
        alpha=.5,
        label=f'prediction uncertainty ({qbin} samples rolling mean)',
    )
    ax.plot(
        Y_[_sort],
        (Y_+dq95)[_sort],
        color='#bbb',
        linewidth=.5,
        alpha=.5,
    )
    ax.set_xlabel('measured')
    ax.set_ylabel('predicted')
    ax.set_title(
        title,
        fontsize=12,
    )
    cb = fig.colorbar(hb, ax=ax)
    cb.set_label('counts')
    ax.legend(loc='upper left', frameon=False)
    plt.savefig(out_file, bbox_inches='tight', dpi=300)
    plt.close()
    del fig, ax


def make_class_plots(Y, cm, target, encoder, out_file_prf):
    n_classes = cm.shape[0]
    fig, ax = plt.subplots(figsize=(5, 5))
    imdata = np.log(cm+1)
    im = ax.imshow(imdata, cmap=alpha_cmap)
    values = np.linspace(imdata.min(), imdata.max(), 10)
    value_labels = (np.e**values - 1).astype(int)
    colors = [im.cmap(im.norm(value)) for value in values]
    patches = [
        mpatches.Patch(color=colors[i], label=str(value_labels[i]))
        for i in range(len(values))
    ]
    ax.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. )
    ax.grid(False)
    ax.set_xlabel('predicted')
    ax.set_ylabel('true')
    ax.set_title(f'{target} classifier confusion matrix')
    ax.set_xticks(range(n_classes))
    ax.set_yticks(range(n_classes))
    ax.set_xticklabels(encoder.classes_, rotation='vertical')
    ax.set_yticklabels(encoder.classes_)
    plt.savefig(out_file_prf+'_confusion.png', bbox_inches='tight', dpi=300)
    plt.close()

    __, counts = np.unique(Y, return_counts=True)
    fig, ax = plt.subplots(figsize=(5, 5))
    imdata = np.log(1 + 100 * cm / counts)
    im = ax.imshow(imdata, cmap=alpha_cmap)
    values = np.log(1 + np.arange(0, 101, 5))
    value_labels = (np.e**values - 1).round(0).astype(int)
    colors = [im.cmap(im.norm(value)) for value in values]
    patches = [
        mpatches.Patch(color=colors[i], label=str(value_labels[i]))
        for i in range(len(values))
    ]
    ax.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. )
    ax.grid(False)
    ax.set_xlabel('predicted')
    ax.set_ylabel('true')
    ax.set_title(f'{target} classifier relative confusion matrix')
    ax.set_xticks(range(n_classes))
    ax.set_yticks(range(n_classes))
    ax.set_xticklabels(encoder.classes_, rotation='vertical')
    ax.set_yticklabels(encoder.classes_)
    plt.savefig(out_file_prf+'_confusion_relative.png', bbox_inches='tight', dpi=300)
    plt.close()