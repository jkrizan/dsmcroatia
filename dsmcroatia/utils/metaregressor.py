
import numpy as np
import sklearn.model_selection as modsel
from sklearn import ensemble, base

class MetaRegressor(base.BaseEstimator, base.RegressorMixin):

    def __init__(self,
        estimators,
        q_alpha=.05, # quantile range
        **kwargs
    ):
        self.weak_estimators = estimators
        self.mean_estimator = ensemble.GradientBoostingRegressor(**kwargs)
        self.lo_estimator = None
        self.hi_estimator = None
        self.y_min = -np.inf
        self.y_max = np.inf
        self.q_alpha = q_alpha

    def get_params(self, *args, **kwargs):
        return self.mean_estimator.get_params(*args, **kwargs)

    def set_params(self, *args, **kwargs):
        self.mean_estimator.set_params(*args, **kwargs)
        self.lo_estimator.set_params(*args, **kwargs)
        self.hi_estimator.set_params(*args, **kwargs)
        return self

    def _estimate(self, X):
        estimated = np.stack([
            estimator.predict(X).reshape(self.target_shape)
            for estimator in self.weak_estimators
        ]).T
        estimated[estimated<self.y_min] = self.y_min
        estimated[estimated>self.y_max] = self.y_max
        return estimated

    def fit(self, X, y, fit_quantile=False, **kwargs):
        self.input_labels = X.columns
        self.target_shape = (-1, *y.shape[1:])
        self.y_min = y.min()
        self.y_max = y.max()
        estimated = self._estimate(X)
        self.mean_estimator.fit(estimated, y, **kwargs)
        if fit_quantile:
            self.lo_estimator = base.clone(self.mean_estimator)
            self.lo_estimator.set_params(loss='quantile', alpha=self.q_alpha)
            self.lo_estimator.fit(estimated, y, **kwargs)
            self.hi_estimator = base.clone(self.mean_estimator)
            self.hi_estimator.set_params(loss='quantile', alpha=1-self.q_alpha)
            self.hi_estimator.fit(estimated, y, **kwargs)
        return self

    def predict(self, X, return_interval=False, **kwargs):
        estimated = self._estimate(X)
        predicted = self.mean_estimator.predict(estimated, **kwargs)
        if return_interval:
            return (
                predicted,
                self.lo_estimator.predict(estimated, **kwargs),
                self.hi_estimator.predict(estimated, **kwargs),
            )
        return predicted

    def cross_validate(self, X, y, **kwargs):
        # try:
        estimated = self._estimate(X)
        return modsel.cross_validate(
            self.mean_estimator,
            estimated, y,
            **kwargs,
        )
        # except:
        #     # kill most important feature eval because of
        #     # constant problems with sklearn and/or xgboost
        #     return np.full((1,), np.nan, np.float32)

