from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
import pandas
from typing import List, Tuple
from dataclasses import dataclass
from pathlib import Path
import pickle

@dataclass
class SubPCA:
    #mask: np.ndarray
    predictors: List
    pca: PCA
    n_comp: int = -1

class MaskedPCA(BaseEstimator, TransformerMixin):
    # 0 < n_components < 1 and svd_solver == 'full'
    pca_kwargs = {'whiten': True, 'n_components':0.9, 'svd_solver': 'full'}
    
    def __init__(self, all_input_columns, pcas_description:dict, nonmasked_columns:List = None, scaling:bool=False):  
        self.nonmasked_columns = nonmasked_columns
        self.masked_columns = None        
        self.out_columns = None
        self.all_input_columns = all_input_columns
                       
        self.scaler = StandardScaler() if scaling else None
        self.pcas={}
        
        self.pcas_description = pcas_description
        self.make_pcas()

        if self.nonmasked_columns is None:
            self.input_columns = self.masked_columns
        else:
            self.input_columns = self.nonmasked_columns + self.masked_columns

        
    def make_pcas(self):
        input_columns= pandas.Index(self.all_input_columns)
        masked_columns=[]
        #nonpca_mask = np.ones(len(self.predictors), dtype=bool)
        for name in self.pcas_description:
            #print(name)
            desc = self.pcas_description[name]
            if not isinstance(desc, Tuple):
                desc=(desc,)
            mask = np.zeros(len(input_columns), dtype=bool)
            for d in desc:
                m = input_columns.str.contains(d)
                mask = mask | m
            pca_predictors = input_columns[mask].to_list()  #sorted(input_columns[mask].to_list())
            masked_columns.extend(pca_predictors)
            self.pcas[name] = SubPCA(pca_predictors, PCA(**self.pca_kwargs))
        
        self.masked_columns=masked_columns


    def fit(self, X, y=0):
        if isinstance(X, pandas.DataFrame):
            X = X[self.masked_columns]
        else:
            X = pandas.DataFrame(X, columns=self.masked_columns)

        if self.scaler is not None:
            X = pandas.DataFrame(self.scaler.fit_transform(X), columns=self.masked_columns)
            
        self.out_columns = []
        for name in self.pcas:
            print(name, end='')
            spca = self.pcas[name]
            x_ = X[spca.predictors]
            mask = x_.isna().any(axis=1).values
            #x_.loc[mask,:] = 0
            spca.pca.fit(x_.loc[~mask,:])
            spca.n_comp = spca.pca.n_components_
            
            self.out_columns.extend([f'{name}_f{i:02}' for i in range(spca.n_comp)])

            print(f',inp_feat: {len(spca.predictors)}, n_comps: {spca.pca.n_components_}, exp_var={spca.pca.explained_variance_ratio_.sum()*100:0.1f}, ncases={(~mask).sum()}')        
        return self

    def transform(self, X, y=0):

        if isinstance(X, pandas.DataFrame):
            X = X[self.input_columns]
        else:
            X = pandas.DataFrame(X, columns=self.input_columns)
        
        if self.nonmasked_columns is not None :
            tX = X[self.nonmasked_columns]
            X = X[self.masked_columns]
        else:
            tX = pandas.DataFrame() 


        if self.scaler is not None:
            X = pandas.DataFrame(self.scaler.transform(X), columns=self.masked_columns)    

        for name in self.pcas:
            spca = self.pcas[name]

            x_ = X[spca.predictors].values
            #mask = x_.isna().any(axis=1).values
            mask = np.isnan(x_).any(axis=1)
            #x_.loc[mask,:] = 0
            x_[mask,:] = 0
            ttX = spca.pca.transform(x_)
            ttX[mask,:] = np.nan
            ttX = pandas.DataFrame(ttX, columns=[f'{name}_f{i:02}' for i in range(ttX.shape[1])])
            tX = pandas.concat((tX,ttX),axis=1, sort=False)

        return tX[self.out_columns]


class PreProcess():
    def __init__(self, fn_model:str):

        #self.ver = ver
        
        self.filename =  fn_model #mdir/f'preproc_{ver}.pickle'
        self.mpca = pickle.load(open(self.filename,'rb'))

        self.num_cols = self.mpca.input_columns

    @property
    def input_columns(self):
        return self.mpca.input_columns

    @property
    def output_columns(self):
        return self.mpca.out_columns

    def process(self, df):
        return self.mpca.transform(df)

    def process_overlay(self, sample_file):
        
        df = pandas.read_csv(sample_file, sep='\t').reset_index(drop=True)

        df1 = df[df.columns[:30]].copy()
        df2 = df[self.input_columns].copy()

        df2 = self.process(df2)

        df = pandas.concat((df1,df2), axis=1, sort=False)

        return df