import rasterio as rio
from pathlib import Path
import os
import psutil
from time import sleep
from multiprocessing.pool import ThreadPool

_MEM_FREE_LIMIT = 2**30
_READ_WAIT_INTERVAL = .01 # seconds

def write_tile(
    data,
    mask,
    window,
    dst_path,
    profile=None,
):
    dst_path = Path(dst_path)

    if dst_path.exists():
        dst = rio.open(dst_path, 'r+')
        profile = dst.profile
    else:
        if profile is None:
            raise ValueError('must specify profile for nonexistent destination file')
        if not dst_path.parent.exists():
            os.makedirs(dst_path.parent, exist_ok=True)
        dst = rio.open(dst_path, 'w', **profile)

    data = data.astype(profile['dtype'])
    data[~mask] = profile['nodata']

    dst.write(data, window=window, indexes=1)
    dst.close()

def read_tile(profile):
    def wrapped(src_path):
        while psutil.virtual_memory().free <= _MEM_FREE_LIMIT:
            sleep(_READ_WAIT_INTERVAL)

        src = rio.open(src_path)
        data = src.read(1)
        mask = src.read_masks(1).astype(bool)
        window = rio.windows.from_bounds(
            *src.bounds,
            transform=profile['transform'],
        )

        return data, mask, window
    return wrapped

def merge_tiles(
    tile_paths,
    dst_path,
    profile,
    verbose=False,
):
    n_tiles = len(tile_paths)

    with ThreadPool(n_tiles) as io_pool:
        for i, (data, mask, window) in enumerate(io_pool.imap_unordered(
            read_tile(profile),
            tile_paths,
        )):
            write_tile(data, mask, window, dst_path, profile)
            if verbose:
                progress = 100 * (i + 1) / n_tiles
                print(f'{round(progress)}% of {dst_path.as_posix()} done', end='\r')
    if verbose:
        print()
