import numpy as np
import rasterio as rio
from rasterio.crs import CRS


extent = [4560000, 2160010, 5070000, 2640010]
# crs = CRS.from_proj4('+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +units=m +no_defs')
crs = CRS.from_epsg('3035')
dx = 30
w = extent[2] - extent[0]
h = extent[3] - extent[1]

assert(not w % dx + h % dx)

shape = (h // dx, w // dx)
transform = rio.Affine(
    dx,  0., extent[0],
    0., -dx, extent[3],
)

transform_full = {
    'crs': crs,
    'width': shape[1],
    'height': shape[0],
    'transform': transform,
}

def raster_get_profile(**kwargs):
    profile = {
        'driver': 'GTiff',
        'dtype': 'int16',
        'nodata': None,
        'count': 1,
        'blockxsize': 512,
        'blockysize': 512,
        'tiled': True,
        'compress': 'deflate',
        **transform_full,
    }
    profile.update(kwargs)
    return profile

def _get_grid_dims(window_dim):
    grid_w = np.ceil(shape[1] / window_dim).astype(int)
    grid_h = np.ceil(shape[0] / window_dim).astype(int)
    return grid_w, grid_h

def raster_n_tiles(window_dim):
    grid_w, grid_h = _get_grid_dims(window_dim)
    return grid_w * grid_h

def make_window(i, window_dim):
    # if i >= grid_w * grid_h:
    #     return None
    grid_w, grid_h = _get_grid_dims(window_dim)
    row = i // grid_w
    col = i % grid_w

    window_w = window_dim
    clip_w = (col + 1) * window_dim - shape[1]
    if clip_w > 0:
        window_w -= clip_w

    window_h = window_dim
    clip_h = (row + 1) * window_dim - shape[0]
    if clip_h > 0:
        window_h -= clip_h

    pos_col = col*window_dim
    pos_row = row*window_dim

    window = rio.windows.Window(pos_col, pos_row, window_w, window_h)
    window_transform = rio.windows.transform(window, transform)

    return window, dict(
        width=window_w,
        height=window_h,
        crs=crs,
        transform=window_transform
    )


