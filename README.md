# DSMCroatia

## Compile points in topsoil database
python compile_input_points.py

- collects input points from all sources, clean and extract topsoil (0-30 cm) properties
- inputs:
  - DSMCroatia input MultiOne.xlsx
  - HAPIH_Ispitivanje plodnosti tla_2019.xlsx
  - Martinovic_WRB_lookup.xlsx
  - Martinovic_WRB2014_lookup.xlsx
  - AZO2016_WRB2014_lookup.xlsx
- outputs:
  - hr_topsoil_db.gpkg
  - hr_topsoil_db.xlsx
  - hr_topsoil_db.csv

## Describe topsoil database
hrtopsoildb_description.ipynb
- describes database and calculates statistics and histograms for report
- inputs:
  - tmp\hr_topsoil_db.pickle

## Sample (overlay) points from topsoil database with predictors
python sample_points.py

## Sample (overlay) for preprocessing
python sample_preprocessing.py

## Make preprocessing model
python make_preprocess.py

## Train models
python train_models.py target ver
- target is name of property or 'all' for all
- ver is version, can be omitted

## Train wrb classificatiuon
python train_wrb.py ver
- ver is version, can be omitted

## Predicting 
python predict.py target ver
- target is name of property or 'all' for all
- ver is version, can be omitted

## Postprocessing soil texture
python postprocess_texture.py ver
- ver is version, can be omitted

## Predicting wrb classification
python predict_wrb.py ver
- ver is version, can be omitted

## Postprocessing classification probability
# creates wrb_rsg_prediction_entropy.tif from wrb_rsg_prob_*.tif probabilities
python postprocess_probability.py ver
- ver is version, can be omitted
