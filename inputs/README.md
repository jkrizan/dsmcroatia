## Compilations of hr_topsoil database

### compile_input_points.py 
- collects input points from all sources, clean and extract topsoil (0-30 cm) properties
- inputs:
  - WBSoilHR project MultiOne.xlsx
  - HAPIH_Ispitivanje plodnosti tla_2019.xlsx
  - Martinovic_WRB_lookup.xlsx
  - Martinovic_WRB2014_lookup.xlsx
  - AZO2016_WRB2014_lookup.xlsx
- outputs:
  - hr_topsoil_db.gpkg
  - hr_topsoil_db.xlsx
  - hr_topsoil_db.csv

### hr_topsoil_db_desc.ipynb
- describes database and calculates statistics and histograms for report
- inputs:
  - tmp\hr_topsoil_db.pickle